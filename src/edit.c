#include "../include/edit.h"
#include <ncurses.h>
#include <string.h>
#include <stdlib.h>
#include "../include/structs/pos-struct.h"
#include "../include/structs/lines-struct.h"
#include "../include/structs/pad-size-struct.h"
#include "../include/input.h"
#include "../include/pad-check.h"
#include "../include/dotfile.h"
#include "../include/search.h"

#define NORMAL_MODE_COLOR COLOR_PAIR(1)
#define EDIT_MODE_COLOR COLOR_PAIR(2)
#define SAVED_COLOR COLOR_PAIR(3)
#define SEARCH_BOX_COLOR COLOR_PAIR(5)

#define DISCARD_INPUT -69 // This is the number I use to mean that the input wasn't a letter, but to move the cursor, (even though the char "-69" has nothing to do with cursor movement) so I should not use it to modify the text file

#define END_KEY 360
#define CTRL_X '\x18'
#define CTRL_W 23
#define CTRL_A 1
#define CTRL_F 6
#define CTRL_N 14

#define NORMAL_MODE 1
#define EDIT_MODE 0

extern struct Pos pos;
extern struct Lines lines;
extern WINDOW* pad;
extern struct PadSize pad_size;
extern struct DotFileData dotfile_data;

void update_cursor_pos()
{
   wmove(pad, pos.y, pos.x);
   refresh_pad();
}

int move_cursor(char* current_text_line)
{
   int input = '0';
   int cursor_moved = 0; // Whether the cursor moved or not
   input = wgetch(pad);

   switch (input)
   {
   case KEY_UP:
      if (pos.y > 0) { pos.y -= 1; }
      cursor_moved = 1;
      break;

   case KEY_LEFT:
      if (pos.x > 0) { pos.x -= 1; }
      cursor_moved = 1;
      break;

   case KEY_DOWN:
      if (pos.y < pad_size.y - 2) { pos.y += 1; }
      cursor_moved = 1;
      break;

   case KEY_RIGHT:
      if (pos.x < pad_size.x - 1) { pos.x += 1; }
      cursor_moved = 1;
      break;

   case 575: // Ctrl+Up_arrow_key
      if (pos.y > dotfile_data.ctrl_up_steps) { pos.y -= dotfile_data.ctrl_up_steps; }
      else { pos.y = 0; }

      cursor_moved = 1;
      break;

   case 554: // Ctrl+Left_arrow_key
      if (pos.x > dotfile_data.ctrl_left_steps) { pos.x -= dotfile_data.ctrl_left_steps; }
      else { pos.x = 0; }

      cursor_moved = 1;
      break;

   case 534: // Ctrl+Down_arrow_key
      if (pos.y + dotfile_data.ctrl_down_steps < pad_size.y - 1) { pos.y += dotfile_data.ctrl_down_steps; }
      else { pos.y = pad_size.y - 3; }

      cursor_moved = 1;
      break;

   case 569: // Ctrl+Right_arrow_key
      if (pos.x + dotfile_data.ctrl_right_steps < pad_size.x - 1) { pos.x += dotfile_data.ctrl_right_steps; }
      else { pos.x = pad_size.x - 2; }

      cursor_moved = 1;
      break;


   case 393: // Shift+Left_arrow_key
      pos.x = 0;
      cursor_moved = 1;
      break;

   case 402: // Shift+Right_arrow_key
      if (current_text_line != NULL) { pos.x = strlen(current_text_line); }
      cursor_moved = 1;
      break;
   }

   update_cursor_pos();

   if (cursor_moved == 0) // If the cursor didn't move (no arrow keys given)
   {
      return input;
   }

   else
   {
      return DISCARD_INPUT;
   }
}


void add_char(char** text, int input)
{
   if (text[pos.y] == NULL) { return; }
   if (is_char_valid(input) == 0) { return; }
   if (pos.y >= lines.current_total) { return; } // User tried to edit a line below the last one
   const int line_length = strlen(text[pos.y]);

   if (line_length + 1 > lines.longest_line_length) // This "+1" after "line_length" represents the new character that will be inserted soon
   {
      lines.longest_line_length = line_length + 1;
   }

   // User tried to type in front of the last char of the line. "strcmp" is there for allowing empty lines
   if (strcmp(text[pos.y], "") == 0 && pos.x != 0) { return; }
   else if (pos.x > line_length && pos.x != 0) { return; }

   text[pos.y] = realloc(text[pos.y], line_length + 3); // Increases the size by 1. I do "strlen + 3" because it doens't count the \0

   int x = pos.x;
   char buffer_char = text[pos.y][x];
   char buffer_char_2 = text[pos.y][x + 1];
   text[pos.y][x] = input;
   for (int i = 1; text[pos.y][x] != '\0'; ++i, ++x)
   {
      if (i % 2 != 0)
      {
         text[pos.y][x + 1] = buffer_char;
         buffer_char = text[pos.y][x + 2];
      }

      else
      {
         text[pos.y][x + 1] = buffer_char_2;
         buffer_char_2 = text[pos.y][x + 2];
      }
   }

   check_pad_size_X(text, text[pos.y], EDIT_MODE);

   // Echoes the change back to the screen
   wmove(pad, pos.y, 0);
   for (int i = 0; i < pos.x; ++i) { wprintw(pad, "%c", ' '); }
   wmove(pad, pos.y, 0);
   wprintw(pad, "%s", text[pos.y]);
   refresh_pad();

   ++pos.x; // Cursor moves 1 to the right after typing in a new char
}

void remove_char(char** text)
{
   if (text[pos.y] == NULL) { return; }
   if (pos.x > strlen(text[pos.y]) && pos.x != 0) { return; } // User tried to erase in front of the last char of the line

   char buffer_char = ' ';
   char buffer_char_2 = ' ';

   // The reason why "i = strlen - 1" and not just "strlen" is because indexing in C is 0 based
   for (int splitter = 1, i = (strlen(text[pos.y]) - 1); i != pos.x - 1; --i, ++splitter)
   {
      if (splitter == 1)
      {
         buffer_char = text[pos.y][i - 1];
         text[pos.y][i - 1] = text[pos.y][i];
      }

      else if (splitter % 2 != 0)
      {
         buffer_char = text[pos.y][i - 1];
         text[pos.y][i - 1] = buffer_char_2;
      }

      else
      {
         buffer_char_2 = text[pos.y][i - 1];
         text[pos.y][i - 1] = buffer_char;
      }
   }

   size_t new_size = strlen(text[pos.y]); // strlen doesn't count the '\0'. So "new_size" will be 1 smaller (as it should)
   text[pos.y][strlen(text[pos.y]) - 1] = '\0';
   text[pos.y] = realloc(text[pos.y], new_size);

   // Echoes the change back to the screen
   wmove(pad, pos.y, 0);
   for (int i = 0; i < strlen(text[pos.y]) + 1; ++i) { wprintw(pad, " "); }
   wmove(pad, pos.y, 0);
   wprintw(pad, "%s", text[pos.y]);
   refresh_pad();

   pos.x -= 1; // Cursor moves to the left after erasing
   update_cursor_pos();
}

void remove_word(char** text)
{
   int position = pos.x;
   int lettersInWord = 0;
   int word_found = 0;

   const int line_length = strlen(text[pos.y]);

   if (position > line_length) { position = line_length; pos.x = position; } // Fixes memory error: "Invalid read of size 1". "pos.x = position" is here for the "remove_char()" call below

   while(position >= 0)
   {
      if (((text[pos.y][position] == ' ') || (text[pos.y][position] == '"') || (text[pos.y][position] == '[') || (text[pos.y][position] == '{') || (text[pos.y][position] == '(')) && (word_found == 1))
      {
         break;
      }

      if ((text[pos.y][position] != '\0') && (text[pos.y][position] != ' ')) { word_found = 1; }
      --position;
      ++lettersInWord;
   }

   for (int i = 0; i < lettersInWord - 1; ++i) // "lettersInWord - 1" is here because the "while" loop above counts one more than needed
   {
      remove_char(text);
   }
}

int add_empty_line(char** text, int total_lines)
{
   if (pos.y >= total_lines) { return 1; } // User should add new lines with NORMAL mode
   total_lines -= 1; // Decreases "total_lines" by 1 because indexing is 0 based

   check_pad_size_Y(text, EDIT_MODE);

   if (text[total_lines + 1] == NULL)
   {
      text[total_lines + 1] = malloc(1); // Allocates memory for a new element
   }

   char* carry_string;
   if (dotfile_data.carry_lines == 1)
   {
      long size = (strlen(text[pos.y]) - pos.x) + 1;
      if (size < 1) { size = 1; } // This prevents supplying calloc with a negative value (this can be negative if "pos.x" is too high). It also prevents another error: "invalid read of size 1" later when I use "strcmp" on "carry_string"
      carry_string = calloc(size, 1);

      for (int i = 0, x = pos.x; i < (long)(strlen(text[pos.y]) - pos.x); ++i, ++x) // This line has the cast to (long) because for some reason it's unsigned by default (so if the value ends up being negative, it overflows back to the max value). I decided to go with "long" rather than "int" to have support for VERY long lines
      {
         carry_string[i] = text[pos.y][x];
      }
   }

   short isCursorOnCorner = 0;
   if (pos.x == 0) { isCursorOnCorner = 1; }

   for (int i = total_lines + 1; i > pos.y; --i)
   {
      if ((i == pos.y + 1) && (isCursorOnCorner == 0))
      {
         strcpy(text[pos.y + 1], "");
      }

      else
      {
         text[i] = realloc(text[i], strlen(text[i - 1]) + 1); // NOT SURE IF THIS LINE IS NECESSARY (WILL TEST LATER)
         strcpy(text[i], text[i - 1]);
      }
   }

   if (isCursorOnCorner == 1)
   {
      strcpy(text[pos.y], "");
   }

   // Reprints "**text" (now that its updated)
   wmove(pad, pos.y, 0);
   for (int i = pos.y; i <= total_lines + 1; ++i)
   {
      wprintw(pad, "%s\n", text[i]);
   }



   // Carries the "carry_string" to the next line
   if ((dotfile_data.carry_lines) == 1 && (strcmp(carry_string, "") != 0))
   {
      const int len = strlen(text[pos.y]);
      wmove(pad, pos.y, pos.x);
      for (int i = pos.x; i < len; ++i)
      {
         text[pos.y][i] = '\0';
         wprintw(pad, " ");
      }

      text[pos.y + 1] = realloc(text[pos.y + 1], (strlen(text[pos.y + 1]) + 1) + strlen(carry_string));
      strcpy(text[pos.y + 1], carry_string); // Copies the "carry_string" to the new line
      pos.x = 0; // Moves the cursor to the left corner (I don't do "pos.y += 1" here because I increase it just after this func finishes executing)
      wmove(pad, pos.y + 1, pos.x);
      wprintw(pad, "%s", carry_string);
      refresh_pad();
   }


   if (dotfile_data.carry_lines == 1) { free(carry_string); }
   if (pos.x == strlen(text[pos.y])) { pos.x = 0; } // Moves the cursor to the left corner
   return 0;
}

int remove_empty_line(char** text, int total_lines)
{
   if (total_lines <= 0) { return 3; }
   if (text[pos.y] == NULL) { return 4; }

   total_lines -= 1; // This is here because indexing is 0 based

   int is_carry = 0;
   char* carry_content;
   if ((pos.x == 0) && (pos.y != 0) && (dotfile_data.carry_lines == 1)) // Carries the contents of the current line to the one above
   {
      is_carry = 1;

      carry_content = calloc(strlen(text[pos.y]) + 1, 1);
      strcpy(carry_content, text[pos.y]);
   }

   else if (strcmp(text[pos.y], "") != 0) { return 5; } // Current line is not empty

   char* buffer;
   char* buffer2;
   if ((strlen(text[pos.y]) == lines.longest_line_length) && (is_carry == 1)) // This "if" is here because if the user carries the longest line, "text[pos.y - 1]" will be longer than "lines.longest_line_length"
   {
      buffer = malloc(lines.longest_line_length + strlen(text[pos.y - 1]));
      buffer2 = malloc(lines.longest_line_length + strlen(text[pos.y - 1]));
   }
   else
   {
      buffer = malloc(lines.longest_line_length);
      buffer2 = malloc(lines.longest_line_length);
   }

   for (int i = total_lines, splitter = 1; i > pos.y; --i, ++splitter)
   {
      if (splitter == 1)
      {
         strcpy(buffer, text[i - 1]);
         text[i - 1] = realloc(text[i - 1], strlen(text[i]) + 1);  // This realloc is here to fix a bug that happens when "text[i - 1]" is longer than "text[i]" (I think. If not this, then vice-versa). If it weren't for this, I would get "invalid write" as one element might have less memory allocated for it than the other
         strcpy(text[i - 1], text[i]);
      }

      else if (splitter % 2 == 0)
      {
         strcpy(buffer2, text[i - 1]);
         text[i - 1] = realloc(text[i - 1], strlen(buffer) + 1); // This realloc is here to fix a bug that happens when "text[i - 1]" is longer than "text[i]" (I think. If not this, then vice-versa). If it weren't for this, I would get "invalid write" as one element might have less memory allocated for it than the other
         strcpy(text[i - 1], buffer);
      }

      else if (splitter % 2 != 0)
      {
         strcpy(buffer, text[i - 1]);
         text[i - 1] = realloc(text[i - 1], strlen(buffer2) + 1);  // This realloc is here to fix a bug that happens when "text[i - 1]" is longer than "text[i]" (I think. If not this, then vice-versa). If it weren't for this, I would get "invalid write" as one element might have less memory allocated for it than the other
         strcpy(text[i - 1], buffer2);
      }
   }

   // text[total_lines] = NULL; // I stopped setting this to NULL because the last "for" in "main()" won't be able to free this then (causing a mem leak). However I'm still leaving this here (commented out) because the fact this is not NULL may cause some behaviour issues on my program
   // free(text[total_lines]); // I removed this free because this will later be freed by the last "for" in "main()"
   free(buffer);
   free(buffer2);

   strcpy(text[lines.current_total - 1], ""); // When removing a line, the last one will always be duplicated. This fixes it

   // Reprints "**text" (now that its updated)
   wmove(pad, pos.y, 0);
   for (int i = pos.y; i < total_lines + 1; ++i)
   {
      wprintw(pad, "%s\n", text[i]);
   }
   refresh_pad();

   if (pos.y == 0) { update_cursor_pos(); } // For some reason, if I erase the first line, the cursor will automatically move to the last line

   if (is_carry == 1) // Carries the line (if needed)
   {
      const int lenght_string = strlen(text[pos.y - 1]);
      text[pos.y - 1] = realloc(text[pos.y - 1], (lenght_string + 1) + (strlen(carry_content)));
      strcat(text[pos.y - 1], carry_content);

      free(carry_content);

      // Reprints the changes to the screen
      lines.longest_line_length = is_longest_line(text[pos.y - 1], lines.longest_line_length);
      check_pad_size_X(text, "", EDIT_MODE);
      wmove(pad, pos.y - 1, 0);
      wprintw(pad, "%s", text[pos.y - 1]);

      pos.x = lenght_string;
   }

   return 0;
}

int edit(char** text, const char* const file_path)
{
   wbkgd(stdscr, EDIT_MODE_COLOR); refresh();
   wbkgd(pad, EDIT_MODE_COLOR); refresh_pad();
   int tmpx = pos.x;
   int save_color = 0; // Whether or not the "save color" is enabled

   for (int input = DISCARD_INPUT; input != CTRL_X;)
   {
      input = move_cursor(text[pos.y]);
      if ((dotfile_data.save_color == 1) && (save_color == 1) && (input != DISCARD_INPUT)) {  save_color = 0; wbkgd(stdscr, EDIT_MODE_COLOR); refresh(); wbkgd(pad, EDIT_MODE_COLOR); refresh_pad(); }


      if (input == END_KEY)
      {
         return END_KEY;
      }

      else if (input == CTRL_A) // Saves the current changes to the file
      {
         FILE* file = fopen(file_path, "w");

         for (int i = 0; i < lines.current_total; ++i)
         {
            fprintf(file, "%s\n", text[i]);
         }

         fclose(file);

         // Changes color
         if (dotfile_data.save_color == 1)
         {
            wbkgd(stdscr, SAVED_COLOR); refresh(); // This is here if the pad is not big enough to cover the entire screen
            wbkgd(pad, SAVED_COLOR); refresh_pad();
            save_color = 1;
         }
      }

      else if (input == CTRL_F)
      {
         search(text);
      }

      else if (input == CTRL_N)
      {
         search_again(text);
      }

      else if (input == CTRL_W)
      {
         remove_word(text);
      }

      else if (backspace_pressed(input))
      {
         if (pos.x != 0)
         {
            remove_char(text);
         }

         else
         {
            int status = remove_empty_line(text, lines.current_total);
            if (status == 0)
            {
               lines.current_total -= 1; // Total amount of lines got decreased
               if (pos.y > 0) // If the user removes the only line left, I don't want to decrease pos.y (or else it would go negative)
               {
                  --pos.y;
                  update_cursor_pos();
               }
            }
         }
      }

      else if ((input != DISCARD_INPUT) && (input != CTRL_X) /*&& (!backspace_pressed(input)) not sure if this is needed. prolly not*/)
      {
         if (input == '\n')
         {
            if (add_empty_line(text, lines.current_total) == 0)
            {
               lines.current_total += 1; lines.peak_total = is_peak_lines_reached(lines.current_total, lines.peak_total);
               ++pos.y;
            }
         }

         else
         {
            add_char(text, input);
         }

         update_cursor_pos();
      }
   }

   pos.y = lines.current_total;
   pos.x = tmpx;
   update_cursor_pos(); // Moves back to the position before the function was called
   wbkgd(stdscr, NORMAL_MODE_COLOR); refresh();
   wbkgd(pad, NORMAL_MODE_COLOR); refresh_pad();
   return DISCARD_INPUT;
}
