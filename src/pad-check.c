#include <ncurses.h>
#include "../include/structs/lines-struct.h"
#include "../include/structs/pad-size-struct.h"
#include "../include/pad-check.h"
#include "../include/input.h"
#include "../include/edit.h"

#define NORMAL_MODE_COLOR COLOR_PAIR(1)
#define EDIT_MODE_COLOR COLOR_PAIR(2)

#define NORMAL_MODE 1
#define EDIT_MODE 0

extern struct Lines lines;
extern struct PadSize pad_size;
extern WINDOW* pad;

void check_pad_size_Y(char** text, int mode)
{
   if (lines.current_total <= (pad_size.y - 2)) { return; }

   int thirty_percent_increase = (pad_size.y * 30) / 100;
   if (thirty_percent_increase < 1) { thirty_percent_increase = 1; } // This line is here if this 30% increase is a floating number that is less than 1
   pad_size.y += thirty_percent_increase;

   delwin(pad);
   pad = newpad(pad_size.y, pad_size.x);
   keypad(pad, true);
   scrollok(pad, TRUE);
   if (mode == NORMAL_MODE) { wbkgd(pad, NORMAL_MODE_COLOR); }
   else if (mode == EDIT_MODE) { wbkgd(pad, EDIT_MODE_COLOR); }

   for (int i = 0; i < lines.current_total; ++i)
   {
      wprintw(pad, "%s\n", text[i]);
   }

   refresh_pad();
}

void check_pad_size_X(char** text, char* current_line, int mode)
{
   if (lines.longest_line_length <= (pad_size.x - 2)) { return; }

   int thirty_percent_increase = (pad_size.x * 30) / 100;
   pad_size.x += thirty_percent_increase;

   if (pad_size.x <= lines.longest_line_length) { pad_size.x = lines.longest_line_length + 5; } // Usually I only add 1 char at a time (so the 30% increase will always suffice), but, several chars can be inserted at a single time when carrying lines in "remove_empty_line()", so this handles it

   delwin(pad);
   pad = newpad(pad_size.y, pad_size.x);
   keypad(pad, TRUE);
   scrollok(pad, TRUE);
   if (mode == NORMAL_MODE) { wbkgd(pad, NORMAL_MODE_COLOR); }
   else if (mode == EDIT_MODE) { wbkgd(pad, EDIT_MODE_COLOR); }

   for (int i = 0; i < lines.current_total; ++i)
   {
      wprintw(pad, "%s\n", text[i]);
   }
   if (mode == NORMAL_MODE) { wprintw(pad, "%s\n", current_line); }

   update_cursor_pos();
}