#include "../include/input.h"
#include <ncurses.h>
#include <string.h>
#include <stdlib.h>
#include "../include/structs/pos-struct.h"
#include "../include/structs/lines-struct.h"
#include "../include/edit.h"
#include "../include/pad-check.h"
#include "../include/dotfile.h"

#define MAX_PAD_SIZE_X 5000 // This macro won't stay forever (>ᴗ•)

#define END_KEY 360
#define CTRL_X '\x18'
#define CTRL_W 23
#define CTRL_A 1

#define NORMAL_MODE_COLOR COLOR_PAIR(1)
#define SAVED_COLOR COLOR_PAIR(3)

#define DISCARD_VALUE -69 // Return value used for nothing

#define NORMAL_MODE 1
#define EDIT_MODE 0

extern struct Pos pos;
extern struct Lines lines;
extern struct DotFileData dotfile_data;
extern WINDOW* pad;

int backspace_pressed(char input)
{
   if ((input == 127) || (input == 7) || (input == 8)) // Some terminal emulators will register "backspace" as 127, some others as 8 and some others as 7
   {
      return 1;
   }

   else
   {
      return 0;
   }
}

void refresh_pad()
{
   int minus_one_COLS = COLS - 1; // In some places, COLS needs to be checked by "-1"

   if (dotfile_data.cursor_centralized == 1 && (lines.current_total > LINES || lines.longest_line_length > COLS) && (pos.y > lines.current_total - LINES / 2 || pos.x > lines.longest_line_length - minus_one_COLS / 2)) // The cursor will smoothly move to the end
   {
      int diffY = 0;
      if (pos.y > lines.current_total - LINES) { diffY = lines.current_total - LINES; }
      else if (pos.y > LINES / 2) { diffY = pos.y - LINES / 2; } // This is here to prevent "diffY" from being 0, in case only "pos.x" is changing

      int diffX = 0;
      if (pos.x > lines.longest_line_length - COLS) { diffX = lines.longest_line_length - minus_one_COLS; }
      else if (pos.x > minus_one_COLS / 2) { diffX = pos.x - minus_one_COLS / 2; } // This is here to prevent "diffX" from being 0, in case only "pos.y" is changing

      prefresh(pad, diffY, diffX, 0, 0, (LINES - 1), (COLS - 1));
   }

   else if (dotfile_data.cursor_centralized == 1 && (pos.y > LINES / 2 || pos.x > minus_one_COLS / 2))
   {
      int diffY = 0;
      if (pos.y > LINES / 2) { diffY = pos.y - LINES / 2; }

      int diffX = 0;
      if (pos.x > minus_one_COLS / 2) { diffX = pos.x - minus_one_COLS / 2; }
      prefresh(pad, diffY, diffX, 0, 0, (LINES - 1), COLS - 1);
   }

   else if ( (dotfile_data.cursor_centralized == 0) && ((pos.y > (LINES - 3)) || (pos.x > minus_one_COLS)) )
   {
      int diffY = 0;
      if (pos.y > (LINES - 3)) { diffY = pos.y - LINES + 2; }

      int diffX = 0;
      if (pos.x > minus_one_COLS) { diffX = pos.x - minus_one_COLS; }
      prefresh(pad, diffY, diffX, 0, 0, LINES - 1, COLS - 1);
   }

   else
   {
      prefresh(pad, 0, 0, 0, 0, LINES - 1, COLS - 1);
   }
}
void refresh_pad_custom(int y, int x)
{
   int minus_one_COLS = COLS - 1; // In some places, COLS needs to be checked by "-1"

   if (dotfile_data.cursor_centralized == 1 && (lines.current_total > LINES || lines.longest_line_length > COLS) && (y > lines.current_total - LINES / 2 || x > lines.longest_line_length - minus_one_COLS / 2)) // The cursor will smoothly move to the end
   {
      int diffY = 0;
      if (y > lines.current_total - LINES) { diffY = lines.current_total - LINES; }
      else if (y > LINES / 2) { diffY = y - LINES / 2; } // This is here to prevent "diffY" from being 0, in case only "x" is changing

      int diffX = 0;
      if (x > lines.longest_line_length - COLS) { diffX = lines.longest_line_length - minus_one_COLS; }
      else if (x > minus_one_COLS / 2) { diffX = x - minus_one_COLS / 2; } // This is here to prevent "diffX" from being 0, in case only "y" is changing

      prefresh(pad, diffY, diffX, 0, 0, (LINES - 1), (COLS - 1));
   }

   else if (dotfile_data.cursor_centralized == 1 && (y > LINES / 2 || x > minus_one_COLS / 2))
   {
      int diffY = 0;
      if (y > LINES / 2) { diffY = y - LINES / 2; }

      int diffX = 0;
      if (x > minus_one_COLS / 2) { diffX = x - minus_one_COLS / 2; }
      prefresh(pad, diffY, diffX, 0, 0, (LINES - 1), COLS - 1);
   }

   else if ( (dotfile_data.cursor_centralized == 0) && ((y > (LINES - 3)) || (x > minus_one_COLS)) )
   {
      int diffY = 0;
      if (y > (LINES - 3)) { diffY = y - LINES + 2; }

      int diffX = 0;
      if (x > minus_one_COLS) { diffX = x - minus_one_COLS; }
      prefresh(pad, diffY, diffX, 0, 0, LINES - 1, COLS - 1);
   }

   else
   {
      prefresh(pad, 0, 0, 0, 0, LINES - 1, COLS - 1);
   }
}

int is_char_valid(int input)
{
   // None of the comments below are things that I'm sure about. For EX, I'm not really sure if "Ctrl+A" will result in "1" on every single terminal emulator, but on the ones I have tested it checks out
   switch(input)
   {
   case '\f':  // F4
      return 0;

   case 2: // Ctrl+B
      return 0;

   case 4: // Ctrl+D
      return 0;

   case 5: // Ctrl+E
      return 0;

   case 6: // Ctrl+F
      return 0;

   case 9: // Ctrl+I
      return 0;

   case 11: // Ctrl+K
      return 0;

   case 14: // Ctrl+N
      return 0;

   case 15: // Ctrl+O
      return 0;

   case 16: // Ctrl+P
      return 0;

   case 18: // Ctrl+R
      return 0;

   case 20: // Ctrl+T
      return 0;

   case 21: // Ctrl+U
      return 0;

   case 22: // Ctrl+V
      return 0;

   case 25: // Ctrl+Y
      return 0;


   case 27: // ESC (escape)
      return 0;


   case 337: // Shift+Up_arrow_key
      return 0;

   case 393: // Shift+Left_arrow_key
      return 0;

   case 336: // Shift+Down_arrow_key
      return 0;

   case 402: // Shift+Right_arrow_key
      return 0;


   case KEY_UP:
      return 0;

   case KEY_LEFT:
      return 0;

   case KEY_DOWN:
      return 0;

   case KEY_RIGHT:
      return 0;


   case 575: // Ctrl+Up_arrow_key
      return 0;

   case 554: // Ctrl+Left_arrow_key
      return 0;

   case 534: // Ctrl+Down_arrow_key
      return 0;

   case 569: // Ctrl+Right_arrow_key
      return 0;
   }

   return 1;
}

int get_line(char** text, char* string, int max_length, char* file_path)
{
   int letter = ' ';
   int save_color = 0; // Whether or not the "save color" is enabled

   // The reason why "i = 1" is because "string[0]" is storing "ctrl_input"
   for (int i = 1; letter != '\n';)
   {
      letter = wgetch(pad);


      if (is_char_valid(letter) == 0) { continue; }
      if ((dotfile_data.save_color) == 1 && (save_color == 1)) { save_color = 0; wbkgd(stdscr, NORMAL_MODE_COLOR); refresh(); wbkgd(pad, NORMAL_MODE_COLOR); refresh_pad(); }

      if (letter == CTRL_A)
      {
         FILE* file = fopen(file_path, "w");

         for (int ii = 0; ii < lines.current_total; ++ii)
         {
            fprintf(file, "%s\n", text[ii]);
         }
         fprintf(file, "%s\n", string);

         fclose(file);


         // Changes color
         if (dotfile_data.save_color == 1)
         {
            wbkgd(stdscr, SAVED_COLOR); refresh(); // This is here if the pad is not big enough to cover the entire screen
            wbkgd(pad, SAVED_COLOR); refresh_pad();
            save_color = 1;
         }
      }

      else if ((letter == CTRL_X) || (letter == END_KEY))
      {
         pos.x = 0;
         ++pos.y;
         update_cursor_pos();

         return letter;
      }

      else if (letter == CTRL_W)
      {
         int posEndOfString = strlen(string) - 1; // This has "-1" for the sake of indexing
         int lettersInWord = 0;

         int word_found = 0;
         while(posEndOfString >= 0)
         {
            if (((string[posEndOfString] == ' ') || (string[posEndOfString] == '"') || (string[posEndOfString] == '[') || (string[posEndOfString] == '{') || (string[posEndOfString] == '(')) && (word_found == 1))
            {
               break;
            }

            if ((string[posEndOfString] != '\0') && (string[posEndOfString] != ' ')) { word_found = 1; }
            --posEndOfString;
            ++lettersInWord;
         }

         for (int ii = 0; ii < lettersInWord; ++ii)
         {
            string[(strlen(string)) - 1] = '\0';
         }

         for (int ii = 0; ii < lettersInWord; ++ii)
         {
            wmove(pad, pos.y, pos.x - 1);
            wprintw(pad, " ");
            --pos.x;
         }

         update_cursor_pos();
         i -= lettersInWord;
      }

      else if ((letter != '\n') && (!backspace_pressed(letter)) && (i < max_length))
      {
         lines.longest_line_length = is_longest_line(string, lines.longest_line_length);
         check_pad_size_X(text, string, NORMAL_MODE);

         wprintw(pad, "%c", letter); // Manually echoing input (only gets echoed if "max_length" is bigger than "i")
         refresh_pad();
         string[i] = letter;
         ++pos.x;
         ++i;
      }

      else if ((backspace_pressed(letter)) && (pos.x != 0))
      {
         size_t new_size = strlen(string) - 1; // "new_size" is actually smaller by 2, since strlen doesn't count the '\0' and also I decrease it by -1
         string[new_size] = '\0'; // "new_size" being smaller by 2 here is perfect, because here I use it to index the string (and indexing is based on 0, so here its techically smaller by 1)

         --pos.x; // Cursor moves to the left after erasing
         --i;

         // Echoes the change back to the screen
         wmove(pad, pos.y, new_size);
         wprintw(pad, " ");
         refresh_pad();

         update_cursor_pos();
      }
   }

   pos.x = 0;
   ++pos.y;
   update_cursor_pos();

   return DISCARD_VALUE;
}

void get_input(char** text, char* file_path)
{
   size_t buffer_size = MAX_PAD_SIZE_X;
   char* buffer = calloc(buffer_size, 1); // Each line can be as long as the maxx of the screen

   int ctrl_input = ' ';
   int save_color = 0; // Whether or not the "save color" is enabled
   while (ctrl_input != END_KEY)
   {
      ctrl_input = wgetch(pad);

      if ((dotfile_data.save_color) == 1 && (save_color == 1)) { save_color = 0; wbkgd(stdscr, NORMAL_MODE_COLOR); refresh(); wbkgd(pad, NORMAL_MODE_COLOR); refresh_pad(); }

      if (ctrl_input == CTRL_A) // Saves the current changes to the file
      {
         FILE* file = fopen(file_path, "w");

         for (int i = 0; i < lines.current_total; ++i)
         {
            fprintf(file, "%s\n", text[i]);
         }

         fclose(file);


         // Changes color
         if (dotfile_data.save_color == 1)
         {
            wbkgd(stdscr, SAVED_COLOR); refresh(); // This is here if the pad is not big enough to cover the entire screen
            wbkgd(pad, SAVED_COLOR); refresh_pad();
            save_color = 1;
         }
      }

      else if ((ctrl_input != CTRL_X) && (ctrl_input != END_KEY) && (ctrl_input != '\n') && (backspace_pressed(ctrl_input) == 0) && (is_char_valid(ctrl_input) == 1) && (ctrl_input != CTRL_W))
      {
         ++pos.x; // Cursor moves one position to the right after user types in a character
         wprintw(pad, "%c", ctrl_input); // Manually echoes ctrl_input
         refresh_pad();
         buffer[0] = ctrl_input;
         ctrl_input = get_line(text, buffer, buffer_size - 1, file_path); // Assigns a value to the buffer (also, if the user types "ctrl+x" then "ctrl_input" will equal '\x18', otherwise "ctrl_input" will equal ' ') (for some reason this needs to be "buffer_size - 1", otherwise it will overflow)

         if (text[lines.current_total] == NULL) // The only scenario I know this WON'T be NULL, is after "remove_empty_line()" executes it will leave some lines with empty-strings
         {
            text[lines.current_total] = calloc(strlen(buffer) + 1, 1);
         }
         else
         {
            text[lines.current_total] = realloc(text[lines.current_total], strlen(buffer) + 1);
         }

         strcpy(text[lines.current_total], buffer);
         memset(buffer, 0, strlen(buffer)); // Empties the buffer, so it's clean on the next "while" iteration
         lines.current_total += 1; lines.peak_total = is_peak_lines_reached(lines.current_total, lines.peak_total);
         check_pad_size_Y(text, NORMAL_MODE);
      }

      if (ctrl_input == CTRL_X)
      {
         ctrl_input = edit(text, file_path);
      }

      else if (ctrl_input == '\n')
      {
         ++pos.y;
         pos.x = 0;
         update_cursor_pos();

         text[lines.current_total] = calloc(1, 1);
         // strcpy(text[lines.current_total], ""); Not sure if this strcpy is needed
         lines.current_total += 1; lines.peak_total = is_peak_lines_reached(lines.current_total, lines.peak_total);
         check_pad_size_Y(text, NORMAL_MODE);
      }
   }

   free(buffer);
}
