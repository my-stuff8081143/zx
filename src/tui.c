#include "../include/tui.h"
#include "../include/dotfile.h"
#include <ncurses.h>
#include <locale.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define NORMAL_MODE_COLOR COLOR_PAIR(1)
#define SELECT_BUTTON_COLOR COLOR_PAIR(4)

extern struct DotFileData dotfile_data;

void print_BACK_btn() { if (LINES >= 27) { move((LINES / 2) + 13, (COLS / 2) - 10); } else { move((LINES / 2) + 7, (COLS / 2) - 2); } printw("Back"); }

void update_select_color_pair() // Creates a new color pair for selected/hovered buttons
{
   int select_foreground_color = 0;
   int select_background_color = 0;

   // Foreground Color
   if (strcmp(dotfile_data.foreground_color, "black") == 0) { select_background_color = COLOR_BLACK; }
   else if (strcmp(dotfile_data.foreground_color, "red") == 0) { select_background_color = COLOR_RED; }
   else if (strcmp(dotfile_data.foreground_color, "green") == 0) { select_background_color = COLOR_GREEN; }
   else if (strcmp(dotfile_data.foreground_color, "yellow") == 0) { select_background_color = COLOR_YELLOW; }
   else if (strcmp(dotfile_data.foreground_color, "blue") == 0) { select_background_color = COLOR_BLUE; }
   else if (strcmp(dotfile_data.foreground_color, "magenta") == 0) { select_background_color = COLOR_MAGENTA; }
   else if (strcmp(dotfile_data.foreground_color, "cyan") == 0) { select_background_color = COLOR_CYAN; }
   else if (strcmp(dotfile_data.foreground_color, "white") == 0) { select_background_color = COLOR_WHITE; }

   // Background color
   if (strcmp(dotfile_data.background_color, "black") == 0) { select_foreground_color = COLOR_BLACK; }
   else if (strcmp(dotfile_data.background_color, "red") == 0) { select_foreground_color = COLOR_RED; }
   else if (strcmp(dotfile_data.background_color, "green") == 0) { select_foreground_color = COLOR_GREEN; }
   else if (strcmp(dotfile_data.background_color, "yellow") == 0) { select_foreground_color = COLOR_YELLOW; }
   else if (strcmp(dotfile_data.background_color, "blue") == 0) { select_foreground_color = COLOR_BLUE; }
   else if (strcmp(dotfile_data.background_color, "magenta") == 0) { select_foreground_color = COLOR_MAGENTA; }
   else if (strcmp(dotfile_data.background_color, "cyan") == 0) { select_foreground_color = COLOR_CYAN; }
   else if (strcmp(dotfile_data.background_color, "white") == 0) { select_foreground_color = COLOR_WHITE; }

   init_pair(4, select_foreground_color, select_background_color);
}

void recreate_dotfile_with_new_setting(short setting, char* value)
{
   char* path = get_dotfile_location("FILE");
   char* rm_command = calloc(3 + strlen(path), 1); strcpy(rm_command, "rm "); strcat(rm_command, path);
   system(rm_command); // Deletes the dotfile in order to update it
   free(rm_command);

   FILE *dotfile = fopen(path, "r"); // I NEED TO CHECK THIS TO SEE IF I REALLY NEED TO START WITH "r" AND NOT "a" STRAIGHT AWAY
   dotfile = fopen(path, "a");

   free(path);

   char* replace_string_bit = calloc(40, 1);
   char* replace_string_full = calloc(375, 1);

   if (setting == 0) // Foreground Color
   {
      strcpy(replace_string_bit, "foreground-color:");
      strcat(replace_string_bit, value);



      char* current_background = calloc(32, 1);
      strcpy(current_background, "\nbackground-color:");
      strcat(current_background, dotfile_data.background_color);

      char* current_edit_foreground = calloc(32, 1);
      strcpy(current_edit_foreground, "\nedit-foreground-color:");
      strcat(current_edit_foreground, dotfile_data.edit_foreground_color); strcat(current_edit_foreground, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "edit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_foreground = calloc(32, 1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "save-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "\nctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_left_steps = calloc(40, 1);
      strcpy(current_ctrl_left_steps, "ctrl-left-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left_steps, num2); strcat(current_ctrl_left_steps, "\n");

      char* current_ctrl_right_steps = calloc(40, 1);
      strcpy(current_ctrl_right_steps, "ctrl-right-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right_steps, num3); strcat(current_ctrl_right_steps, "\n");

      char* current_ctrl_down_steps = calloc(40, 1);
      strcpy(current_ctrl_down_steps, "ctrl-down-steps:");
      char num4[20]; sprintf(num4, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down_steps, num4); strcat(current_ctrl_down_steps, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");



      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, current_edit_foreground);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, current_ctrl_left_steps);
      strcat(replace_string_full, current_ctrl_right_steps);
      strcat(replace_string_full, current_ctrl_down_steps);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_background);
      free(current_edit_foreground);
      free(current_edit_background);
      free(current_save_foreground);
      free(current_save_background);
      free(current_ctrl_up_steps);
      free(current_ctrl_left_steps);
      free(current_ctrl_right_steps);
      free(current_ctrl_down_steps);
      free(current_carry_lines);
      free(current_save_color);
      free(current_cursor_centralized);

      dotfile_data.foreground_color = value;
   }

   else if (setting == 1) // Background Color
   {
      strcpy(replace_string_bit, "background-color:");
      strcat(replace_string_bit, value);



      char* current_foreground = calloc(32, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_edit_foreground = calloc(32, 1);
      strcpy(current_edit_foreground, "\nedit-foreground-color:");
      strcat(current_edit_foreground, dotfile_data.edit_foreground_color); strcat(current_edit_foreground, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "edit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_foreground = calloc(32, 1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "save-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "\nctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_left_steps = calloc(40, 1);
      strcpy(current_ctrl_left_steps, "ctrl-left-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left_steps, num2); strcat(current_ctrl_left_steps, "\n");

      char* current_ctrl_right_steps = calloc(40, 1);
      strcpy(current_ctrl_right_steps, "ctrl-right-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right_steps, num3); strcat(current_ctrl_right_steps, "\n");

      char* current_ctrl_down_steps = calloc(40, 1);
      strcpy(current_ctrl_down_steps, "ctrl-down-steps:");
      char num4[20]; sprintf(num4, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down_steps, num4); strcat(current_ctrl_down_steps, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");



      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, current_edit_foreground);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, current_ctrl_left_steps);
      strcat(replace_string_full, current_ctrl_right_steps);
      strcat(replace_string_full, current_ctrl_down_steps);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_foreground);
      free(current_edit_foreground);
      free(current_edit_background);
      free(current_save_foreground);
      free(current_save_background);
      free(current_ctrl_up_steps);
      free(current_ctrl_left_steps);
      free(current_ctrl_right_steps);
      free(current_ctrl_down_steps);
      free(current_carry_lines);
      free(current_save_color);
      free(current_cursor_centralized);

      dotfile_data.background_color = value;
   }

   else if (setting == 2) // EDIT Foreground Color
   {
      strcpy(replace_string_bit, "edit-foreground-color:");
      strcat(replace_string_bit, value);



      char* current_foreground = calloc(32, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_background = calloc(32, 1);
      strcpy(current_background, "background-color:");
      strcat(current_background, dotfile_data.background_color); strcat(current_background, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "\nedit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_foreground = calloc(32 ,1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "save-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "\nctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_left = calloc(40, 1);
      strcpy(current_ctrl_left, "ctrl-left-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left, num2); strcat(current_ctrl_left, "\n");

      char* current_ctrl_right_steps = calloc(40, 1);
      strcpy(current_ctrl_right_steps, "ctrl-right-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right_steps, num3); strcat(current_ctrl_right_steps, "\n");

      char* current_ctrl_down_steps = calloc(40, 1);
      strcpy(current_ctrl_down_steps, "ctrl-down-steps:");
      char num4[20]; sprintf(num4, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down_steps, num4); strcat(current_ctrl_down_steps, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");



      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, current_ctrl_left);
      strcat(replace_string_full, current_ctrl_right_steps);
      strcat(replace_string_full, current_ctrl_down_steps);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_foreground);
      free(current_background);
      free(current_edit_background);
      free(current_save_foreground);
      free(current_save_background);
      free(current_ctrl_up_steps);
      free(current_ctrl_left);
      free(current_ctrl_right_steps);
      free(current_ctrl_down_steps);
      free(current_carry_lines);
      free(current_save_color);
      free(current_cursor_centralized);

      dotfile_data.edit_foreground_color = value;
   }

   else if (setting == 3) // EDIT background color
   {
      strcpy(replace_string_bit, "edit-background-color:");
      strcat(replace_string_bit, value);
      strcat(replace_string_bit, "\n");



      char* current_foreground = calloc(32, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_background = calloc(32, 1);
      strcpy(current_background, "background-color:");
      strcat(current_background, dotfile_data.background_color); strcat(current_background, "\n");

      char* current_edit_foreground = calloc(32, 1);
      strcpy(current_edit_foreground, "edit-foreground-color:");
      strcat(current_edit_foreground, dotfile_data.edit_foreground_color); strcat(current_edit_foreground, "\n");

      char* current_save_foreground = calloc(32, 1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "save-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "\nctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_left_steps = calloc(40, 1);
      strcpy(current_ctrl_left_steps, "ctrl-left-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left_steps, num2); strcat(current_ctrl_left_steps, "\n");

      char* current_ctrl_right_steps = calloc(40, 1);
      strcpy(current_ctrl_right_steps, "ctrl-right-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right_steps, num3); strcat(current_ctrl_right_steps, "\n");

      char* current_ctrl_down_steps = calloc(40, 1);
      strcpy(current_ctrl_down_steps, "ctrl-down-steps:");
      char num4[20]; sprintf(num4, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down_steps, num4); strcat(current_ctrl_down_steps, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");



      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, current_edit_foreground);
      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, current_ctrl_left_steps);
      strcat(replace_string_full, current_ctrl_right_steps);
      strcat(replace_string_full, current_ctrl_down_steps);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_foreground);
      free(current_background);
      free(current_edit_foreground);
      free(current_save_foreground);
      free(current_save_background);
      free(current_ctrl_up_steps);
      free(current_ctrl_left_steps);
      free(current_ctrl_right_steps);
      free(current_ctrl_down_steps);
      free(current_carry_lines);
      free(current_save_color);
      free(current_cursor_centralized);

      dotfile_data.edit_background_color = value;
   }

   else if (setting == 4) // Ctrl Up Steps
   {
      strcpy(replace_string_bit, "ctrl-up-steps:");
      strcat(replace_string_bit, value);
      strcat(replace_string_bit, "\n");



      char* current_foreground = calloc(32, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_background = calloc(32, 1);
      strcpy(current_background, "background-color:");
      strcat(current_background, dotfile_data.background_color); strcat(current_background, "\n");

      char* current_edit_foregroud = calloc(32, 1);
      strcpy(current_edit_foregroud, "edit-foreground-color:");
      strcat(current_edit_foregroud, dotfile_data.edit_foreground_color); strcat(current_edit_foregroud, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "edit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_foreground = calloc(32, 1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "save-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_left = calloc(40, 1);
      strcpy(current_ctrl_left, "ctrl-left-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left, num); strcat(current_ctrl_left, "\n");

      char* current_ctrl_right = calloc(40, 1);
      strcpy(current_ctrl_right, "ctrl-right-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right, num2); strcat(current_ctrl_right, "\n");

      char* current_ctrl_down = calloc(40, 1);
      strcpy(current_ctrl_down, "ctrl-down-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down, num3); strcat(current_ctrl_down, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");



      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, current_edit_foregroud);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, current_ctrl_left);
      strcat(replace_string_full, current_ctrl_right);
      strcat(replace_string_full, current_ctrl_down);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_foreground);
      free(current_background);
      free(current_edit_foregroud);
      free(current_edit_background);
      free(current_save_foreground);
      free(current_save_background);
      free(current_ctrl_left);
      free(current_ctrl_right);
      free(current_ctrl_down);
      free(current_carry_lines);
      free(current_save_color);
      free(current_cursor_centralized);

      dotfile_data.ctrl_up_steps = atoi(value);
   }

   else if (setting == 5) // Ctrl Left Steps
   {
      strcpy(replace_string_bit, "ctrl-left-steps:");
      strcat(replace_string_bit, value);
      strcat(replace_string_bit, "\n");



      char* current_foreground = calloc(27, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_background = calloc(27, 1);
      strcpy(current_background, "background-color:");
      strcat(current_background, dotfile_data.background_color); strcat(current_background, "\n");

      char* current_edit_foreground = calloc(32, 1);
      strcpy(current_edit_foreground, "edit-foreground-color:");
      strcat(current_edit_foreground, dotfile_data.edit_foreground_color); strcat(current_edit_foreground, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "edit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_foreground = calloc(32, 1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "save-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "ctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_right_steps = calloc(40, 1);
      strcpy(current_ctrl_right_steps, "ctrl-right-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right_steps, num2); strcat(current_ctrl_right_steps, "\n");

      char* current_ctrl_down_steps = calloc(40, 1);
      strcpy(current_ctrl_down_steps, "ctrl-down-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down_steps, num3); strcat(current_ctrl_down_steps, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");



      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, current_edit_foreground);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, current_ctrl_right_steps);
      strcat(replace_string_full, current_ctrl_down_steps);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_foreground);
      free(current_background);
      free(current_edit_foreground);
      free(current_edit_background);
      free(current_save_foreground);
      free(current_save_background);
      free(current_ctrl_up_steps);
      free(current_ctrl_right_steps);
      free(current_ctrl_down_steps);
      free(current_carry_lines);
      free(current_save_color);
      free(current_cursor_centralized);

      dotfile_data.ctrl_left_steps = atoi(value);
   }

   else if (setting == 6) // Ctrl Right Steps
   {
      strcpy(replace_string_bit, "ctrl-right-steps:");
      strcat(replace_string_bit, value);
      strcat(replace_string_bit, "\n");



      char* current_foreground = calloc(27, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_background = calloc(27, 1);
      strcpy(current_background, "background-color:");
      strcat(current_background, dotfile_data.background_color); strcat(current_background, "\n");

      char* current_edit_foreground = calloc(32, 1);
      strcpy(current_edit_foreground, "edit-foreground-color:");
      strcat(current_edit_foreground, dotfile_data.edit_foreground_color); strcat(current_edit_foreground, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "edit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_foreground = calloc(32, 1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "save-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "ctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_left_steps = calloc(40, 1);
      strcpy(current_ctrl_left_steps, "ctrl-left-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left_steps, num2); strcat(current_ctrl_left_steps, "\n");

      char* current_ctrl_down_steps = calloc(40, 1);
      strcpy(current_ctrl_down_steps, "ctrl-down-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down_steps, num3); strcat(current_ctrl_down_steps, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");


      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, current_edit_foreground);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, current_ctrl_left_steps);
      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, current_ctrl_down_steps);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_foreground);
      free(current_background);
      free(current_edit_foreground);
      free(current_edit_background);
      free(current_save_foreground);
      free(current_save_background);
      free(current_ctrl_up_steps);
      free(current_ctrl_left_steps);
      free(current_ctrl_down_steps);
      free(current_carry_lines);
      free(current_save_color);
      free(current_cursor_centralized);

      dotfile_data.ctrl_right_steps = atoi(value);
   }

   else if (setting == 7) // Ctrl Down Steps
   {
      strcpy(replace_string_bit, "ctrl-down-steps:");
      strcat(replace_string_bit, value);
      strcat(replace_string_bit, "\n");



      char* current_foreground = calloc(27, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_background = calloc(27, 1);
      strcpy(current_background, "background-color:");
      strcat(current_background, dotfile_data.background_color); strcat(current_background, "\n");

      char* current_edit_foreground = calloc(32, 1);
      strcpy(current_edit_foreground, "edit-foreground-color:");
      strcat(current_edit_foreground, dotfile_data.edit_foreground_color); strcat(current_edit_foreground, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "edit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_foreground = calloc(32, 1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "save-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "ctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_left_steps = calloc(40, 1);
      strcpy(current_ctrl_left_steps, "ctrl-left-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left_steps, num2); strcat(current_ctrl_left_steps, "\n");

      char* current_ctrl_right_steps = calloc(40, 1);
      strcpy(current_ctrl_right_steps, "ctrl-right-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right_steps, num3); strcat(current_ctrl_right_steps, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");



      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, current_edit_foreground);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, current_ctrl_left_steps);
      strcat(replace_string_full, current_ctrl_right_steps);
      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_foreground);
      free(current_background);
      free(current_edit_foreground);
      free(current_edit_background);
      free(current_save_foreground);
      free(current_save_background);
      free(current_ctrl_up_steps);
      free(current_ctrl_left_steps);
      free(current_ctrl_right_steps);
      free(current_carry_lines);
      free(current_save_color);
      free(current_cursor_centralized);

      dotfile_data.ctrl_down_steps = atoi(value);
   }

   else if (setting == 8) // Carry lines
   {
      strcpy(replace_string_bit, "carry-lines:");
      strcat(replace_string_bit, value);
      strcat(replace_string_bit, "\n");



      char* current_foreground = calloc(27, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_background = calloc(27, 1);
      strcpy(current_background, "background-color:");
      strcat(current_background, dotfile_data.background_color); strcat(current_background, "\n");

      char* current_edit_foreground = calloc(32, 1);
      strcpy(current_edit_foreground, "edit-foreground-color:");
      strcat(current_edit_foreground, dotfile_data.edit_foreground_color); strcat(current_edit_foreground, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "edit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_foreground = calloc(32, 1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "save-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "ctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_left_steps = calloc(40, 1);
      strcpy(current_ctrl_left_steps, "ctrl-left-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left_steps, num2); strcat(current_ctrl_left_steps, "\n");

      char* current_ctrl_right_steps = calloc(40, 1);
      strcpy(current_ctrl_right_steps, "ctrl-right-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right_steps, num3); strcat(current_ctrl_right_steps, "\n");

      char* current_ctrl_down_steps = calloc(40, 1);
      strcpy(current_ctrl_down_steps, "ctrl-down-steps:");
      char num4[20]; sprintf(num4, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down_steps, num4); strcat(current_ctrl_down_steps, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");



      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, current_edit_foreground);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, current_ctrl_left_steps);
      strcat(replace_string_full, current_ctrl_right_steps);
      strcat(replace_string_full, current_ctrl_down_steps);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_foreground);
      free(current_background);
      free(current_edit_foreground);
      free(current_edit_background);
      free(current_save_foreground);
      free(current_save_background);
      free(current_ctrl_up_steps);
      free(current_ctrl_left_steps);
      free(current_ctrl_right_steps);
      free(current_ctrl_down_steps);
      free(current_save_color);
      free(current_cursor_centralized);

      if (strcmp("true", value) == 0) { dotfile_data.carry_lines = 1; }
      else { dotfile_data.carry_lines = 0; }
   }

   else if (setting == 9) // Save Color
   {
      strcpy(replace_string_bit, "save-color:");
      strcat(replace_string_bit, value);
      strcat(replace_string_bit, "\n");



      char* current_foreground = calloc(27, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_background = calloc(27, 1);
      strcpy(current_background, "background-color:");
      strcat(current_background, dotfile_data.background_color); strcat(current_background, "\n");

      char* current_edit_foreground = calloc(32, 1);
      strcpy(current_edit_foreground, "edit-foreground-color:");
      strcat(current_edit_foreground, dotfile_data.edit_foreground_color); strcat(current_edit_foreground, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "edit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_foreground = calloc(32, 1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "save-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "ctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_left_steps = calloc(40, 1);
      strcpy(current_ctrl_left_steps, "ctrl-left-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left_steps, num2); strcat(current_ctrl_left_steps, "\n");

      char* current_ctrl_right_steps = calloc(40, 1);
      strcpy(current_ctrl_right_steps, "ctrl-right-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right_steps, num3); strcat(current_ctrl_right_steps, "\n");

      char* current_ctrl_down_steps = calloc(40, 1);
      strcpy(current_ctrl_down_steps, "ctrl-down-steps:");
      char num4[20]; sprintf(num4, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down_steps, num4); strcat(current_ctrl_down_steps, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");



      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, current_edit_foreground);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, current_ctrl_left_steps);
      strcat(replace_string_full, current_ctrl_right_steps);
      strcat(replace_string_full, current_ctrl_down_steps);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_foreground);
      free(current_background);
      free(current_edit_foreground);
      free(current_edit_background);
      free(current_save_foreground);
      free(current_save_background);
      free(current_ctrl_up_steps);
      free(current_ctrl_left_steps);
      free(current_ctrl_right_steps);
      free(current_ctrl_down_steps);
      free(current_carry_lines);
      free(current_cursor_centralized);

      if (strcmp("true", value) == 0) { dotfile_data.save_color = 1; }
      else { dotfile_data.save_color = 0; }
   }

   else if (setting == 10) // Save Foreground Color
   {
      strcpy(replace_string_bit, "save-foreground-color:");
      strcat(replace_string_bit, value);



      char* current_foreground = calloc(32, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_background = calloc(32, 1);
      strcpy(current_background, "background-color:");
      strcat(current_background, dotfile_data.background_color); strcat(current_background, "\n");

      char* current_edit_foreground = calloc(32, 1);
      strcpy(current_edit_foreground, "edit-foreground-color:");
      strcat(current_edit_foreground, dotfile_data.edit_foreground_color); strcat(current_edit_foreground, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "edit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "\nsave-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "\nctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_left_steps = calloc(40, 1);
      strcpy(current_ctrl_left_steps, "ctrl-left-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left_steps, num2); strcat(current_ctrl_left_steps, "\n");

      char* current_ctrl_right_steps = calloc(40, 1);
      strcpy(current_ctrl_right_steps, "ctrl-right-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right_steps, num3); strcat(current_ctrl_right_steps, "\n");

      char* current_ctrl_down_steps = calloc(40, 1);
      strcpy(current_ctrl_down_steps, "ctrl-down-steps:");
      char num4[20]; sprintf(num4, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down_steps, num4); strcat(current_ctrl_down_steps, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");



      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, current_edit_foreground);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, current_ctrl_left_steps);
      strcat(replace_string_full, current_ctrl_right_steps);
      strcat(replace_string_full, current_ctrl_down_steps);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_foreground);
      free(current_background);
      free(current_edit_foreground);
      free(current_edit_background);
      free(current_save_background);
      free(current_ctrl_up_steps);
      free(current_ctrl_left_steps);
      free(current_ctrl_right_steps);
      free(current_ctrl_down_steps);
      free(current_carry_lines);
      free(current_save_color);
      free(current_cursor_centralized);

      dotfile_data.save_foreground_color = value;
   }

   else if (setting == 11) // Save background color
   {
      strcpy(replace_string_bit, "save-background-color:");
      strcat(replace_string_bit, value);
      strcat(replace_string_bit, "\n");



      char* current_foreground = calloc(32, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_background = calloc(32, 1);
      strcpy(current_background, "background-color:");
      strcat(current_background, dotfile_data.background_color); strcat(current_background, "\n");

      char* current_edit_foreground = calloc(32, 1);
      strcpy(current_edit_foreground, "edit-foreground-color:");
      strcat(current_edit_foreground, dotfile_data.edit_foreground_color); strcat(current_edit_foreground, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "edit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_foreground = calloc(32, 1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "\nctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_left_steps = calloc(40, 1);
      strcpy(current_ctrl_left_steps, "ctrl-left-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left_steps, num2); strcat(current_ctrl_left_steps, "\n");

      char* current_ctrl_right_steps = calloc(40, 1);
      strcpy(current_ctrl_right_steps, "ctrl-right-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right_steps, num3); strcat(current_ctrl_right_steps, "\n");

      char* current_ctrl_down_steps = calloc(40, 1);
      strcpy(current_ctrl_down_steps, "ctrl-down-steps:");
      char num4[20]; sprintf(num4, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down_steps, num4); strcat(current_ctrl_down_steps, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");

      char* current_cursor_centralized = calloc(30, 1);
      strcpy(current_cursor_centralized, "\ncursor-centralized:");
      if (dotfile_data.cursor_centralized == 1) { strcat(current_cursor_centralized, "true"); }
      else { strcat(current_cursor_centralized, "false"); }
      strcat(current_cursor_centralized, "\n");



      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, current_edit_foreground);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, current_ctrl_left_steps);
      strcat(replace_string_full, current_ctrl_right_steps);
      strcat(replace_string_full, current_ctrl_down_steps);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, replace_string_bit);
      strcat(replace_string_full, current_cursor_centralized);

      free(current_foreground);
      free(current_background);
      free(current_edit_foreground);
      free(current_edit_background);
      free(current_save_foreground);
      free(current_ctrl_up_steps);
      free(current_ctrl_left_steps);
      free(current_ctrl_right_steps);
      free(current_ctrl_down_steps);
      free(current_carry_lines);
      free(current_save_color);
      free(current_cursor_centralized);

      dotfile_data.save_background_color = value;
   }

   else if (setting == 12) // Cursor Centralized
   {
      strcpy(replace_string_bit, "cursor-centralized:");
      strcat(replace_string_bit, value);
      strcat(replace_string_bit, "\n");



      char* current_foreground = calloc(32, 1);
      strcpy(current_foreground, "foreground-color:");
      strcat(current_foreground, dotfile_data.foreground_color); strcat(current_foreground, "\n");

      char* current_background = calloc(32, 1);
      strcpy(current_background, "background-color:");
      strcat(current_background, dotfile_data.background_color); strcat(current_background, "\n");

      char* current_edit_foreground = calloc(32, 1);
      strcpy(current_edit_foreground, "edit-foreground-color:");
      strcat(current_edit_foreground, dotfile_data.edit_foreground_color); strcat(current_edit_foreground, "\n");

      char* current_edit_background = calloc(32, 1);
      strcpy(current_edit_background, "edit-background-color:");
      strcat(current_edit_background, dotfile_data.edit_background_color); strcat(current_edit_background, "\n");

      char* current_save_foreground = calloc(32, 1);
      strcpy(current_save_foreground, "save-foreground-color:");
      strcat(current_save_foreground, dotfile_data.save_foreground_color); strcat(current_save_foreground, "\n");

      char* current_save_background = calloc(32, 1);
      strcpy(current_save_background, "save-background-color:");
      strcat(current_save_background, dotfile_data.save_background_color); strcat(current_save_background, "\n");

      char* current_ctrl_up_steps = calloc(40, 1);
      strcpy(current_ctrl_up_steps, "\nctrl-up-steps:");
      char num[20]; sprintf(num, "%d", dotfile_data.ctrl_up_steps); strcat(current_ctrl_up_steps, num); strcat(current_ctrl_up_steps, "\n");

      char* current_ctrl_left_steps = calloc(40, 1);
      strcpy(current_ctrl_left_steps, "ctrl-left-steps:");
      char num2[20]; sprintf(num2, "%d", dotfile_data.ctrl_left_steps); strcat(current_ctrl_left_steps, num2); strcat(current_ctrl_left_steps, "\n");

      char* current_ctrl_right_steps = calloc(40, 1);
      strcpy(current_ctrl_right_steps, "ctrl-right-steps:");
      char num3[20]; sprintf(num3, "%d", dotfile_data.ctrl_right_steps); strcat(current_ctrl_right_steps, num3); strcat(current_ctrl_right_steps, "\n");

      char* current_ctrl_down_steps = calloc(40, 1);
      strcpy(current_ctrl_down_steps, "ctrl-down-steps:");
      char num4[20]; sprintf(num4, "%d", dotfile_data.ctrl_down_steps); strcat(current_ctrl_down_steps, num4); strcat(current_ctrl_down_steps, "\n");

      char* current_carry_lines = calloc(19, 1);
      strcpy(current_carry_lines, "carry-lines:");
      if (dotfile_data.carry_lines == 1) { strcat(current_carry_lines, "true"); }
      else { strcat(current_carry_lines, "false"); }
      strcat(current_carry_lines, "\n");

      char* current_save_color = calloc(19, 1);
      strcpy(current_save_color, "save-color:");
      if (dotfile_data.save_color == 1) { strcat(current_save_color, "true"); }
      else { strcat(current_save_color, "false"); }
      strcat(current_save_color, "\n");



      strcat(replace_string_full, current_foreground);
      strcat(replace_string_full, current_background);
      strcat(replace_string_full, current_edit_foreground);
      strcat(replace_string_full, current_edit_background);
      strcat(replace_string_full, current_ctrl_up_steps);
      strcat(replace_string_full, current_ctrl_left_steps);
      strcat(replace_string_full, current_ctrl_right_steps);
      strcat(replace_string_full, current_ctrl_down_steps);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_carry_lines);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, current_save_color);
      strcat(replace_string_full, current_save_foreground);
      strcat(replace_string_full, current_save_background);
      strcat(replace_string_full, "\n");
      strcat(replace_string_full, replace_string_bit);

      free(current_foreground);
      free(current_background);
      free(current_edit_foreground);
      free(current_edit_background);
      free(current_save_foreground);
      free(current_save_background);
      free(current_ctrl_up_steps);
      free(current_ctrl_left_steps);
      free(current_ctrl_right_steps);
      free(current_ctrl_down_steps);
      free(current_carry_lines);
      free(current_save_color);

      if (strcmp(value, "true") == 0) { dotfile_data.cursor_centralized = 1; }
      else { dotfile_data.cursor_centralized = 0; }
   }

   fprintf(dotfile, "%s", replace_string_full);

   free(replace_string_full);
   free(replace_string_bit);
   fclose(dotfile);
}

void print_BLACK_btn() { move((LINES / 2) - 4, (COLS / 2) - 2); printw("Black"); }
void print_RED_btn() { move((LINES / 2) - 3, (COLS / 2) - 1); printw("Red"); }
void print_GREEN_btn() { move((LINES / 2) - 2, (COLS / 2) - 2); printw("Green"); }
void print_YELLOW_btn() { move((LINES / 2) - 1, (COLS / 2) - 3); printw("Yellow"); }
void print_BLUE_btn() { move((LINES / 2), (COLS / 2) - 2); printw("Blue"); }
void print_MAGENTA_btn() { move((LINES / 2) + 1, (COLS / 2) - 3); printw("Magenta"); }
void print_CYAN_btn() { move((LINES / 2) + 2, (COLS / 2) - 2); printw("Cyan"); }
void print_WHITE_btn() { move((LINES / 2) + 3, (COLS / 2) - 2); printw("White"); }
void display_color_selection_menu(short setting)
{
   clear();
   attrset(NORMAL_MODE_COLOR);



   // Prints all the buttons
   print_BLACK_btn();
   print_RED_btn();
   print_GREEN_btn();
   print_YELLOW_btn();
   print_BLUE_btn();
   print_MAGENTA_btn();
   print_CYAN_btn();
   print_WHITE_btn();
   print_BACK_btn();



   // Gives the user control over selecting buttons
   attrset(SELECT_BUTTON_COLOR);
   print_BLACK_btn();
   short current_btn = 0;

   int input = 0;
   while (true)
   {
      input = getch();

      // Movement
      if ((input == KEY_DOWN) && (current_btn == 0))
      {
         attrset(NORMAL_MODE_COLOR);
         print_BLACK_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_RED_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 1))
      {
         attrset(NORMAL_MODE_COLOR);
         print_RED_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_GREEN_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 2))
      {
         attrset(NORMAL_MODE_COLOR);
         print_GREEN_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_YELLOW_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 3))
      {
         attrset(NORMAL_MODE_COLOR);
         print_YELLOW_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_BLUE_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 4))
      {
         attrset(NORMAL_MODE_COLOR);
         print_BLUE_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_MAGENTA_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 5))
      {
         attrset(NORMAL_MODE_COLOR);
         print_MAGENTA_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CYAN_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 6))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CYAN_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_WHITE_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 7))
      {
         attrset(NORMAL_MODE_COLOR);
         print_WHITE_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_BACK_btn();

         ++current_btn;
      }

      else if ((input == KEY_UP) && (current_btn == 8))
      {
         attrset(NORMAL_MODE_COLOR);
         print_BACK_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_WHITE_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 7))
      {
         attrset(NORMAL_MODE_COLOR);
         print_WHITE_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CYAN_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 6))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CYAN_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_MAGENTA_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 5))
      {
         attrset(NORMAL_MODE_COLOR);
         print_MAGENTA_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_BLUE_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 4))
      {
         attrset(NORMAL_MODE_COLOR);
         print_BLUE_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_YELLOW_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 3))
      {
         attrset(NORMAL_MODE_COLOR);
         print_YELLOW_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_GREEN_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 2))
      {
         attrset(NORMAL_MODE_COLOR);
         print_GREEN_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_RED_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 1))
      {
         attrset(NORMAL_MODE_COLOR);
         print_RED_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_BLACK_btn();

         --current_btn;
      }



      // Enter presses
      else if ((input == '\n') && (current_btn == 0))
      {
         recreate_dotfile_with_new_setting(setting, "black");

         setup_color_pairs();
         update_select_color_pair();
         refresh();
      }
      else if ((input == '\n') && (current_btn == 1))
      {
         recreate_dotfile_with_new_setting(setting, "red");

         setup_color_pairs();
         update_select_color_pair();
         refresh();
      }
      else if ((input == '\n') && (current_btn == 2))
      {
         recreate_dotfile_with_new_setting(setting, "green");

         setup_color_pairs();
         update_select_color_pair();
         refresh();
      }
      else if ((input == '\n') && (current_btn == 3))
      {
         recreate_dotfile_with_new_setting(setting, "yellow");

         setup_color_pairs();
         update_select_color_pair();
         refresh();
      }
      else if ((input == '\n') && (current_btn == 4))
      {
         recreate_dotfile_with_new_setting(setting, "blue");

         setup_color_pairs();
         update_select_color_pair();
         refresh();
      }
      else if ((input == '\n') && (current_btn == 5))
      {
         recreate_dotfile_with_new_setting(setting, "magenta");

         setup_color_pairs();
         update_select_color_pair();
         refresh();
      }
      else if ((input == '\n') && (current_btn == 6))
      {
         recreate_dotfile_with_new_setting(setting, "cyan");

         setup_color_pairs();
         update_select_color_pair();
         refresh();
      }
      else if ((input == '\n') && (current_btn == 7))
      {
         recreate_dotfile_with_new_setting(setting, "white");

         setup_color_pairs();
         update_select_color_pair();
         refresh();
      }

      else if ((input == '\n') && (current_btn == 8))
      {
         break;
      }
   }
}

void get_ctrl_steps(short setting)
{
   clear();
   attrset(NORMAL_MODE_COLOR);
   echo();
   wmove(stdscr, (LINES / 2) - 2, (COLS / 2) - 8);
   wprintw(stdscr, "Type in a number:");

   wmove(stdscr, LINES / 2, (COLS / 2) - 1);

   char* number_string = calloc(20, 1);
   getstr(number_string);

   recreate_dotfile_with_new_setting(setting, number_string);

   free(number_string);
   attrset(SELECT_BUTTON_COLOR);
   noecho();
}

void print_TRUE_btn()
{
   wmove(stdscr, (LINES / 2) - 1, (COLS / 2) - 2);
   wprintw(stdscr, "True");
}
void print_FALSE_btn()
{
   wmove(stdscr, (LINES / 2) + 1, (COLS / 2) - 2);
   wprintw(stdscr, "False");
}
void display_bool_selection_menu(short setting)
{
   clear();

   attrset(NORMAL_MODE_COLOR);
   print_FALSE_btn();
   print_BACK_btn();

   attrset(SELECT_BUTTON_COLOR);
   print_TRUE_btn();

   int input = 0;
   short current_btn = 0;

   while (true)
   {
      input = getch();



      // Movement
      if ((input == KEY_DOWN) && (current_btn == 0))
      {
         attrset(NORMAL_MODE_COLOR);
         print_TRUE_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_FALSE_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 1))
      {
         attrset(NORMAL_MODE_COLOR);
         print_FALSE_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_BACK_btn();

         ++current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 1))
      {
         attrset(NORMAL_MODE_COLOR);
         print_FALSE_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_TRUE_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 2))
      {
         attrset(NORMAL_MODE_COLOR);
         print_BACK_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_FALSE_btn();

         --current_btn;
      }



      // Enter Presses
      else if ((input == '\n') && (current_btn == 0))
      {
         recreate_dotfile_with_new_setting(setting, "true");
      }
      else if ((input == '\n') && (current_btn == 1))
      {
         recreate_dotfile_with_new_setting(setting, "false");
      }
      else if ((input == '\n') && (current_btn == 2))
      {
         break;
      }
   }
}

void print_FOREGROUND_COLOR_btn() { if (LINES >= 27) { move((LINES / 2) - 11, (COLS / 2) - 8); } else { move((LINES / 2) - 7, (COLS / 2) - 8); } printw("Foreground Color"); }
void print_BACKGROUND_COLOR_btn() { if (LINES >= 27) { move((LINES / 2) - 10, (COLS / 2) - 8); } else { move((LINES / 2) - 6, (COLS / 2) - 8); } printw("Background Color"); }
void print_EDIT_FOREGROUND_COLOR_btn() { if (LINES >= 27) { move((LINES / 2) - 7, (COLS / 2) - 10); } else { move((LINES / 2) - 5, (COLS / 2) - 10); } printw("EDIT Foreground Color"); }
void print_EDIT_BACKGROUND_COLOR_btn() { if (LINES >= 27) { move((LINES / 2) - 6, (COLS / 2) - 10); } else { move((LINES / 2) - 4, (COLS / 2) - 10); } printw("EDIT Background Color"); }
void print_CTRLUP_btn() { if (LINES >= 27) { move((LINES / 2) - 3, (COLS / 2) - 6); } else { move((LINES / 2) - 3, (COLS / 2) - 6); } printw("CTRL Up Steps"); }
void print_CTRLLEFT_btn() { if (LINES >= 27) { move(LINES / 2 - 2, (COLS / 2) - 7); } else { move((LINES / 2) - 2, (COLS / 2) - 7); } printw("CTRL Left Steps"); }
void print_CTRLRIGHT_btn() { if (LINES >= 27) { move((LINES / 2) - 1, (COLS / 2) - 8); } else { move((LINES / 2) - 1, (COLS / 2) - 8); } printw("CTRL Right Steps"); }
void print_CTRLDOWN_btn() { if (LINES >= 27) { move(LINES / 2, (COLS / 2) - 7); } else { move((LINES / 2), (COLS / 2) - 7); } printw("CTRL Down Steps"); }
void print_CARRY_LINES_btn() { if (LINES >= 27) { move((LINES / 2) + 3, (COLS / 2) - 5); } else { move((LINES / 2) + 1, (COLS / 2) - 5); } printw("Carry Lines"); }
void print_SAVE_COLOR_btn() { if (LINES >= 27) { move((LINES / 2) + 6, (COLS / 2) - 5); } else { move((LINES / 2) + 2, (COLS / 2) - 5); } printw("Save Color"); }
void print_SAVE_FOREGROUND_COLOR_btn() { if (LINES >= 27) { move((LINES / 2) + 7, (COLS / 2) - 10); } else { move((LINES / 2) + 3, (COLS / 2) - 10); } printw("Save Foreground Color"); }
void print_SAVE_BACKGROUND_COLOR_btn() { if (LINES >= 27) { move((LINES / 2) + 8, (COLS / 2) - 10); } else { move((LINES / 2) + 4, (COLS / 2) - 10); } printw("Save Background Color"); }
void print_CURSOR_CENTRALIZED_btn() { if (LINES >= 27) { move((LINES / 2) + 10, (COLS / 2) - 9); } else { move((LINES / 2) + 5, (COLS / 2) - 9); } printw("Cursor Centralized"); }
void display_settings()
{
   short current_btn = 0; // "0" = Foreground Color; 1 = "Background Color"; 2 = EDIT Foreground Color; 3 = EDIT Background Color; 4 = Ctrl UP steps; 5 = Ctrl Left Steps; 6 = Ctrl Right Steps; 7 = Ctrl Right Steps; 8 = Carry Lines; 9 = Save Color; 10 = Save Foreground Color; 11 = Save Background Color; 12 = Cursor Centralized; 13 = Back
   repeat_display_settings:

   clear();
   attrset(NORMAL_MODE_COLOR);



   // Prints the buttons
   print_FOREGROUND_COLOR_btn();
   print_BACKGROUND_COLOR_btn();

   print_EDIT_FOREGROUND_COLOR_btn();
   print_EDIT_BACKGROUND_COLOR_btn();

   print_CTRLUP_btn();
   print_CTRLLEFT_btn();
   print_CTRLRIGHT_btn();
   print_CTRLDOWN_btn();

   print_CARRY_LINES_btn();

   print_SAVE_COLOR_btn();
   print_SAVE_FOREGROUND_COLOR_btn();
   print_SAVE_BACKGROUND_COLOR_btn();

   print_CURSOR_CENTRALIZED_btn();

   print_BACK_btn();



   // Colors the current Selected/Hovered button with the "SELECT_BUTTON_COLOR" color
   attrset(SELECT_BUTTON_COLOR);
   switch (current_btn)
   {
   case 0:
      print_FOREGROUND_COLOR_btn();
      break;

   case 1:
      print_BACKGROUND_COLOR_btn();
      break;

   case 2:
      print_EDIT_FOREGROUND_COLOR_btn();
      break;

   case 3:
      print_EDIT_BACKGROUND_COLOR_btn();
      break;

   case 4:
      print_CTRLUP_btn();
      break;

   case 5:
      print_CTRLLEFT_btn();
      break;

   case 6:
      print_CTRLRIGHT_btn();
      break;

   case 7:
      print_CTRLDOWN_btn();
      break;

   case 8:
      print_CARRY_LINES_btn();
      break;

   case 9:
      print_SAVE_COLOR_btn();
      break;

   case 10:
      print_SAVE_FOREGROUND_COLOR_btn();
      break;

   case 11:
      print_SAVE_BACKGROUND_COLOR_btn();
      break;

   case 12:
      print_CURSOR_CENTRALIZED_btn();
      break;
   }

   // Gives the user control over selecting buttons
   int input = 0;
   while (true)
   {
      input = getch();

      // Movement
      if ((input == KEY_DOWN) && (current_btn == 0))
      {
         attrset(NORMAL_MODE_COLOR);
         print_FOREGROUND_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_BACKGROUND_COLOR_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 1))
      {
         attrset(NORMAL_MODE_COLOR);
         print_BACKGROUND_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_EDIT_FOREGROUND_COLOR_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 2))
      {
         attrset(NORMAL_MODE_COLOR);
         print_EDIT_FOREGROUND_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_EDIT_BACKGROUND_COLOR_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 3))
      {
         attrset(NORMAL_MODE_COLOR);
         print_EDIT_BACKGROUND_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CTRLUP_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 4))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CTRLUP_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CTRLLEFT_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 5))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CTRLLEFT_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CTRLRIGHT_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 6))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CTRLRIGHT_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CTRLDOWN_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 7))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CTRLDOWN_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CARRY_LINES_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 8))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CARRY_LINES_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_SAVE_COLOR_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 9))
      {
         attrset(NORMAL_MODE_COLOR);
         print_SAVE_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_SAVE_FOREGROUND_COLOR_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 10))
      {
         attrset(NORMAL_MODE_COLOR);
         print_SAVE_FOREGROUND_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_SAVE_BACKGROUND_COLOR_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 11))
      {
         attrset(NORMAL_MODE_COLOR);
         print_SAVE_BACKGROUND_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CURSOR_CENTRALIZED_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 12))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CURSOR_CENTRALIZED_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_BACK_btn();

         ++current_btn;
      }

      else if ((input == KEY_UP) && (current_btn == 13))
      {
         attrset(NORMAL_MODE_COLOR);
         print_BACK_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CURSOR_CENTRALIZED_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 12))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CURSOR_CENTRALIZED_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_SAVE_BACKGROUND_COLOR_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 11))
      {
         attrset(NORMAL_MODE_COLOR);
         print_SAVE_BACKGROUND_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_SAVE_FOREGROUND_COLOR_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 10))
      {
         attrset(NORMAL_MODE_COLOR);
         print_SAVE_FOREGROUND_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_SAVE_COLOR_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 9))
      {
         attrset(NORMAL_MODE_COLOR);
         print_SAVE_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CARRY_LINES_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 8))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CARRY_LINES_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CTRLDOWN_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 7))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CTRLDOWN_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CTRLRIGHT_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 6))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CTRLRIGHT_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CTRLLEFT_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 5))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CTRLLEFT_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_CTRLUP_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 4))
      {
         attrset(NORMAL_MODE_COLOR);
         print_CTRLUP_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_EDIT_BACKGROUND_COLOR_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 3))
      {
         attrset(NORMAL_MODE_COLOR);
         print_EDIT_BACKGROUND_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_EDIT_FOREGROUND_COLOR_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 2))
      {
         attrset(NORMAL_MODE_COLOR);
         print_EDIT_FOREGROUND_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_BACKGROUND_COLOR_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 1))
      {
         attrset(NORMAL_MODE_COLOR);
         print_BACKGROUND_COLOR_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_FOREGROUND_COLOR_btn();

         --current_btn;
      }



      // Enter presses
      else if ((input == '\n') && (current_btn == 0))
      {
         display_color_selection_menu(0);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn) == 1)
      {
         display_color_selection_menu(1);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn == 2))
      {
         display_color_selection_menu(2);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn == 3))
      {
         display_color_selection_menu(3);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn == 4))
      {
         get_ctrl_steps(4);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn == 5))
      {
         get_ctrl_steps(5);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn) == 6)
      {
         get_ctrl_steps(6);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn == 7))
      {
         get_ctrl_steps(7);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn == 8))
      {
         display_bool_selection_menu(8);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn == 9))
      {
         display_bool_selection_menu(9);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn == 10))
      {
         display_color_selection_menu(10);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn == 11))
      {
         display_color_selection_menu(11);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn == 12))
      {
         display_bool_selection_menu(12);

         goto repeat_display_settings;
      }
      else if ((input == '\n') && (current_btn == 13))
      {
         attrset(NORMAL_MODE_COLOR);
         break;
      }
   }
}

void print_OPEN_FILE_btn() { move(LINES / 2, (COLS / 2) - 7); printw("OPEN TEXT FILE"); } // Prints the "OPEN TEXT FILE" button
void print_SETTINGS_btn() { move(LINES / 2 + 3, (COLS / 2) - 4); printw("SETTINGS"); } // Prints the "SETTINGS" button
void print_QUIT_btn() { move(LINES / 2 + 6, (COLS / 2) - 2); printw("QUIT"); } // Prints the "QUIT" button
short display_tui()
{
   setlocale(LC_ALL, "");
   initscr();
   curs_set(0);
   start_color();
   if ((LINES <= 14) || (COLS <= 20))
   {
      wmove(stdscr, LINES / 2, (COLS / 2) - 5);
      wprintw(stdscr, "window smol");
      getch();

      endwin();
      exit(1);
   }
   if (check_dotfile() != 0) { return -1; }



   repeat_display_tui:

   read_dotfile();
   setup_color_pairs();
   wbkgd(stdscr, NORMAL_MODE_COLOR);



   // Draws the logo
   if ((LINES > 19) && (COLS > 25))
   {
      move((LINES / 2) - 10, (COLS / 2) - 8);
      printw("███████╗██╗  ██╗");

      move((LINES / 2) - 9, (COLS / 2) - 8);
      printw("╚══███╔╝╚██╗██╔╝");

      move((LINES / 2) - 8, (COLS / 2) - 8);
      printw("  ███╔╝  ╚███╔╝");

      move((LINES / 2) - 7, (COLS / 2) - 8);
      printw(" ███╔╝   ██╔██╗");

      move((LINES / 2) - 6, (COLS / 2) - 8);
      printw("███████╗██╔╝ ██╗");

      move((LINES / 2) - 5, (COLS / 2) - 8);
      printw("╚══════╝╚═╝  ╚═╝");
   }
   else
   {
      move((LINES / 2) - 5, (COLS / 2) - 1);
      printw("ZX");
   }


   update_select_color_pair();


   // Prints all buttons
   print_OPEN_FILE_btn();
   print_SETTINGS_btn();
   print_QUIT_btn();



   // Gives the user control over selecting buttons
   attrset(SELECT_BUTTON_COLOR);
   print_OPEN_FILE_btn();
   short current_btn = 0; // 0 = "NEW FILE"; 1 = "SETTINGS"; 2 = "QUIT"

   noecho();
   keypad(stdscr, 1);
   int input = 0;
   while (input != 'q')
   {
      input = getch();

      // Movement
      if ((input == KEY_DOWN) && (current_btn == 0))
      {
         attrset(NORMAL_MODE_COLOR);
         print_OPEN_FILE_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_SETTINGS_btn();

         ++current_btn;
      }
      else if ((input == KEY_DOWN) && (current_btn == 1))
      {
         attrset(NORMAL_MODE_COLOR);
         print_SETTINGS_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_QUIT_btn();

         ++current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 2))
      {
         attrset(NORMAL_MODE_COLOR);
         print_QUIT_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_SETTINGS_btn();

         --current_btn;
      }
      else if ((input == KEY_UP) && (current_btn == 1))
      {
         attrset(NORMAL_MODE_COLOR);
         print_SETTINGS_btn();

         attrset(SELECT_BUTTON_COLOR);
         print_OPEN_FILE_btn();

         --current_btn;
      }


      // Enter presses
      else if ((input == '\n') && (current_btn == 0))
      {
         return 0;
      }
      else if ((input == '\n') && (current_btn == 1))
      {
         display_settings();

         clear();
         goto repeat_display_tui;
      }
      else if ((input == '\n') && (current_btn == 2))
      {
         return 2;
      }
   }

   return 2; // This line should only trigger if the user ever presses 'q', which will close the program. Since it behaves just like the "QUIT" button, I return the same value (2)
}
