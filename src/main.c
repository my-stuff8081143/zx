#include <ncurses.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include "../include/input.h"
#include "../include/structs/pos-struct.h"
#include "../include/structs/lines-struct.h"
#include "../include/structs/pad-size-struct.h"
#include "../include/dotfile.h"
#include "../include/tui.h"
#include "../include/edit.h"

// These 2 macros won't stay forever (>ᴗ•)
#define MAX_PAD_SIZE_Y 5000
#define MAX_PAD_SIZE_X 5000

#define NORMAL_MODE_COLOR COLOR_PAIR(1)
#define EDIT_MODE_COLOR COLOR_PAIR(2)

#define NORMAL_MODE 1
#define EDIT_MODE 0

#define END_KEY 360

struct Pos pos = { 0, 0 };
struct Lines lines = { 0, 0 };
WINDOW* pad;
struct PadSize pad_size = { 10, 50 };
struct DotFileData dotfile_data = { };
char* search_buffer;

void write_to_file(char** text, FILE* file, char* file_path)
{
   file = fopen(file_path, "w"); // Empties up the file in case there's stuff on it already

   for (int i = 0; i < lines.current_total; ++i)
   {
      fprintf(file, "%s\n", text[i]);
   }
}


int main(int argc, char **argv)
{
   short TUI_triggered = 0; // 0 if the TUI wasn't triggered; 1 if the TUI WAS triggered
   if (argc != 2)
   {
      short btn_pressed = display_tui();

      if (btn_pressed == 2 || btn_pressed == -1) // "2" is the "QUIT" button. "-1" is an error, not an actual button
      {
         endwin();
         return 0;
      }

      attrset(NORMAL_MODE_COLOR);
      clear();
      TUI_triggered = 1;
      curs_set(1);
   }



   char* file_path = argv[1];
   if (TUI_triggered == 1)
   {
      file_path = calloc(1000, 1); // I use "calloc" on this pointer because "getstr" needs a heap pointer

      wmove(stdscr, (LINES / 2) - 2, (COLS / 2) - 10);
      wprintw(stdscr, "TYPE THE FILE'S PATH:");

      wmove(stdscr, (LINES / 2), (COLS / 2) - 4);
      echo();
      getstr(file_path);
      clear();
      noecho();
   }

   FILE* file = fopen(file_path, "a+");
   if (file == NULL)
   {
      if (TUI_triggered == 0)
      {
         fprintf(stderr, "ERROR: IT SEEMS LIKE YOU TRIED TO OPEN A FOLDER OR AN INVALID FILE!\n");
         exit(2);
      }

      else
      {
         curs_set(0);

         wmove(stdscr, LINES / 2, (COLS / 2) - 33);
         wprintw(stdscr, "%s", "ERROR: IT SEEMS LIKE YOU TRIED TO OPEN A FOLDER OR AN INVALID FILE!");

         getch();
         endwin();
         exit(2);
      }
   }


   // Basic Ncurses initialization
   if (TUI_triggered == 0) // This "if" is here because "display_tui()" already does these things
   {
      if (check_dotfile() != 0)
      {
         fclose(file);
         endwin();
         return 2;
      }
      read_dotfile();
      setlocale(LC_ALL, "");
      initscr();
      noecho();
      curs_set(1);
      start_color();
      setup_color_pairs(); // Sets up NORMAL mode and EDIT mode color pairs

      if ((LINES <= 14) || (COLS <= 20))
      {
         wmove(stdscr, LINES / 2, (COLS / 2) - 5);
         wprintw(stdscr, "window smol");
         getch();

         endwin();
         return 1;
      }
   }

   pad = newpad(pad_size.y, pad_size.x);
   keypad(pad, TRUE);
   scrollok(pad, TRUE);
   bkgd(NORMAL_MODE_COLOR); refresh(); // This line will set and refresh "stdscr" to the color. The reason why this must be done, is that the pad may be smaller than the window
   wbkgd(pad, NORMAL_MODE_COLOR);

   char** text = calloc(1000000, 1); // Will store the entire content of the text file

   // Reads the file and assigns its data to "text"
   short file_empty = 1; // This is used to check whether I should open the file in NORMAL mode or in EDIT mode at the start of the program
   char* check_if_empty = calloc(1, 1);
   if (fread(check_if_empty, 1, 1, file) != 0) // File is not empty
   {
      file_empty = 0;
      char* line_of_text = calloc(MAX_PAD_SIZE_X, 1);
      ungetc(check_if_empty[0], file); // Puts first read char back into the stream

      for (int i = 0; fgets(line_of_text, MAX_PAD_SIZE_X, file) != NULL; ++i)
      {
         const int current_line_length = strlen(line_of_text);

         text[i] = calloc(current_line_length + 1, 1); // "strlen + 1" is here because it doesn't count the '\n'
         strcpy(text[i], line_of_text);


         if (current_line_length > lines.longest_line_length)
         {
            lines.longest_line_length = current_line_length;
         }

         if (text[i][current_line_length - 1] == '\n') // Removes '\n' at the end. There's the "-1" because indexing is 0 based
         {
            text[i][current_line_length - 1] = '\0';
         }


         lines.current_total += 1;
      }
      lines.peak_total = is_peak_lines_reached(lines.current_total, lines.peak_total);

      free(line_of_text);

      pad_size.y = lines.current_total + 2; // This has the "+2" because of the 1st "if" in "check_pad_size_Y()". If not for this, it would get triggered right on the first check
      pad_size.x = lines.longest_line_length + 2; // This has the "+2" because of the 1st "if" in "check_pad_size_X()". If not for this, it would get triggered right on the first check
      delwin(pad);
      pad = newpad(pad_size.y, pad_size.x);
      if (pad == NULL)
      {
         printw("ERROR: IT SEEMS LIKE THE FILE YOU ARE ATTEMPTING TO EDIT IS TOO BIG!");
         getch();

         free(check_if_empty);

         for (int i = 0; i < lines.peak_total; ++i)
         {
            free(text[i]);
         }

         free(text);
         fclose(file);
         delwin(pad);
         endwin();
         exit(3);
      }
      keypad(pad, TRUE);
      scrollok(pad, TRUE);
      wbkgd(pad, NORMAL_MODE_COLOR);


      // Prints the file's text to the screen
      for (int i = 0; i < lines.current_total; ++i) { wprintw(pad, "%s\n", text[i]); }
      refresh_pad();
      pos.y = lines.current_total;
   }
   free(check_if_empty);

   search_buffer = calloc(1, 1);

   if (file_empty == 0) // If the file is not empty, we start on EDIT mode
   {
      pos.y = 0; pos.x = 0; update_cursor_pos();
      int edit_input = edit(text, file_path);

      if (edit_input == END_KEY) { goto end_program; }
   }

   get_input(text, file_path);

   end_program:
   write_to_file(text, file, file_path);

   for (int i = 0; i < lines.peak_total; ++i)
   {
      free(text[i]);
   }

   if (TUI_triggered == 1) { free(file_path); }
   free(dotfile_data.foreground_color); free(dotfile_data.background_color); free(dotfile_data.edit_foreground_color); free(dotfile_data.edit_background_color); free(dotfile_data.save_background_color);
   free(text);
   free(search_buffer);
   fclose(file);
   delwin(pad);
   endwin();
   return 0;
}
