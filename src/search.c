#include "../include/search.h"
#include <ncurses.h>
#include <string.h>
#include <stdlib.h>
#include "../include/structs/pos-struct.h"
#include "../include/structs/lines-struct.h"
#include "../include/input.h"
#include "../include/edit.h"


#define NORMAL_MODE_COLOR COLOR_PAIR(1)
#define EDIT_MODE_COLOR COLOR_PAIR(2)
#define SEARCH_BOX_COLOR COLOR_PAIR(5)
#define SEARCH_BOX_COLOR_FG COLOR_PAIR(6)

#define SEARCH_BOX_SIZE_Y 5
#define SEARCH_BOX_SIZE_X 50

#define CTRL_X '\x18'
#define CTRL_W 23
#define CTRL_A 1
#define CTRL_F 6
#define ESC 27

extern struct Pos pos;
extern struct Lines lines;
extern WINDOW* pad;
extern struct PadSize pad_size;
extern struct DotFileData dotfile_data;
extern char* search_buffer;

short draw_search_box()
{
   wattrset(pad, SEARCH_BOX_COLOR);



   // Here I draw the box on the middle of the screen, handling all 9 states.
   // The vertical position has 3 states; The horizontal position also has 3 states. 3 * 3 = 9. (the three states being: "beggining"; "center"; "ending")

                                       // B = "beggining"; C = "center"; E = "ending"
   // "possition 0" = "BB"; position 1 = "BC"; position 2 = "BE"
                                                   // "position 3" = "CB"; position 4 = "CC"; position 5 = "CE"
                                                   // "position 6" = "EB"; position 7 = "EC"; position 8 = "EE"
                                                   //
   short position = 0;

   // B, C and E boolean expressions ("*" can be either X (horizontal) or Y(vertical)):
   // 01- Beggning check = "pos* < Window*Size / 2"
   // 02- Center check = "pos* > Window*Size / 2 && pos* < WindowCorner - Window*Size / 2"
   // 03- End check = "pos* > WindowCorner - Window*Size / 2"

   if (pos.y < LINES / 2 && pos.x < COLS / 2) { position = 0; } // BB
   else if (pos.y < LINES / 2 && (pos.x > COLS / 2 && pos.x < lines.longest_line_length - COLS / 2)) { position = 1; } // BC
   else if (pos.y < LINES / 2 && pos.x > lines.longest_line_length - COLS / 2) { position = 2; } // BE

   else if ((pos.y > LINES / 2 && pos.y < lines.current_total - LINES / 2) && pos.x < COLS / 2) { position = 3; } // CB
   else if ((pos.y > LINES / 2 && pos.y < lines.current_total - LINES / 2) && (pos.x > COLS / 2 && pos.x < lines.longest_line_length - COLS / 2)) { position = 4; } // CC
   else if ((pos.y > LINES / 2 && pos.y < lines.current_total - LINES / 2) && pos.x > lines.longest_line_length - COLS / 2) { position = 5; } // CE

   else if (pos.y > lines.current_total - LINES / 2 && pos.x < COLS / 2) { position = 6; } // EB
   else if (pos.y > lines.current_total - LINES / 2 && (pos.x > COLS / 2 && pos.x < lines.longest_line_length - COLS / 2)) { position = 7; } // EC
   else if (pos.y > lines.current_total - LINES / 2 && pos.x > lines.longest_line_length - COLS / 2) { position = 8; } // EE



   // Draws the box depending on where the cursor currently is
   const short box_sizeY = SEARCH_BOX_SIZE_Y;
   const short box_sizeX = SEARCH_BOX_SIZE_X;

   int move_expressionY = 0;
   int move_expressionX = 0;


   if (position == 0) // BB
   {
      move_expressionY = LINES / 2;
      move_expressionX = COLS / 2 - box_sizeX / 2;
   }
   else if (position == 1) // BC
   {
      move_expressionY = LINES / 2;
      move_expressionX = pos.x - box_sizeX / 2; // Here I use "pos.x" since it will always be centralized
   }
   else if (position == 2) // BE
   {
      move_expressionY = LINES / 2;
      move_expressionX = lines.longest_line_length - COLS / 2 - box_sizeX / 2;
   }

   else if (position == 3) // CB
   {
      move_expressionY = pos.y; // Here I use "pos.y" since it will always be centralized
      move_expressionX = COLS / 2 - box_sizeX / 2;
   }
   else if (position == 4) // CC
   {
      move_expressionY = pos.y;
      move_expressionX = pos.x - box_sizeX / 2;
   }
   else if (position == 5) // CE
   {
      move_expressionY = pos.y;
      move_expressionX = lines.longest_line_length - COLS / 2 - box_sizeX / 2;
   }

   else if (position == 6) // EB
   {
      move_expressionY = lines.current_total - LINES / 2;
      move_expressionX = COLS / 2 - box_sizeX / 2;
   }
   else if (position == 7) // EC
   {
      move_expressionY = lines.current_total - LINES / 2;
      move_expressionX = pos.x - box_sizeX / 2;
   }
   else if (position == 8) // EE
   {
      move_expressionY = lines.current_total - LINES / 2;
      move_expressionX = lines.longest_line_length - COLS / 2 - box_sizeX / 2;
   }


   for (int i = 0, up = 0, down = 1; i < box_sizeY; ++i) // Draws the box ("up" and "down" here are used to properly center the box)
   {
      if (i % 2 == 0)
      { 
         wmove(pad, move_expressionY + up, move_expressionX);
         ++up;
      }
      else if (i % 2 != 0)
      { 
         wmove(pad, move_expressionY - down, move_expressionX);
         ++down;
      }


      for (int ii = 0; ii < box_sizeX; ++ii) { wprintw(pad, " "); }
   }


   wattrset(pad, EDIT_MODE_COLOR);
   const int y = pos.y;
   const int x = pos.x;
   pos.y = move_expressionY; pos.x = move_expressionX + 1;
   wmove(pad, pos.y, pos.x); // Moves the cursor inside the box, readying for text input
   refresh_pad_custom(y, x); // This is here so that the "camera" doesn't move when drawing the box

   return position;
}

void erase_search_box(char** text, short position)
{
   // Draws the box depending on where the cursor currently is
   const short box_sizeY = SEARCH_BOX_SIZE_Y;
   const short box_sizeX = SEARCH_BOX_SIZE_X;

   int move_expressionY = 0;
   int move_expressionX = 0;


   if (position == 0) // BB
   {
      move_expressionY = LINES / 2;
      move_expressionX = COLS / 2 - box_sizeX / 2;
   }
   else if (position == 1) // BC
   {
      move_expressionY = LINES / 2;
      move_expressionX = pos.x - box_sizeX / 2; // Here I use "pos.x" since it will always be centralized
   }
   else if (position == 2) // BE
   {
      move_expressionY = LINES / 2;
      move_expressionX = lines.longest_line_length - COLS / 2 - box_sizeX / 2;
   }

   else if (position == 3) // CB
   {
      move_expressionY = pos.y; // Here I use "pos.y" since it will always be centralized
      move_expressionX = COLS / 2 - box_sizeX / 2;
   }
   else if (position == 4) // CC
   {
      move_expressionY = pos.y;
      move_expressionX = pos.x - box_sizeX / 2;
   }
   else if (position == 5) // CE
   {
      move_expressionY = pos.y;
      move_expressionX = lines.longest_line_length - COLS / 2 - box_sizeX / 2;
   }

   else if (position == 6) // EB
   {
      move_expressionY = lines.current_total - LINES / 2;
      move_expressionX = COLS / 2 - box_sizeX / 2;
   }
   else if (position == 7) // EC
   {
      move_expressionY = lines.current_total - LINES / 2;
      move_expressionX = pos.x - box_sizeX / 2;
   }
   else if (position == 8) // EE
   {
      move_expressionY = lines.current_total - LINES / 2;
      move_expressionX = lines.longest_line_length - COLS / 2 - box_sizeX / 2;
   }


   for (int i = 0; i < box_sizeY; ++i) // Erases the box
   {
      int x = move_expressionX;

      wmove(pad, move_expressionY + i, move_expressionX);
      for (int ii = 0; ii < box_sizeX; ++ii)
      {
        if (strlen(text[move_expressionY + i]) > x) { wprintw(pad, "%c", text[move_expressionY + i][x]); }
        else { wprintw(pad, " "); }

        ++x;
      }
   }



   refresh_pad();
}


void search(char** text)
{
   memset(search_buffer, 0, strlen(search_buffer) + 1);
   int search_buffer_size = 11;
   search_buffer = realloc(search_buffer, search_buffer_size);

   // Saves the current value of "pos.y" and of "pos.x" before "draw_search_box()" changes them
   const int y = pos.y;
   const int x = pos.x;
   short position = draw_search_box();

   wattrset(pad, SEARCH_BOX_COLOR_FG);
   for (int i = 0, letter = 0; letter != '\n';)
   {
      letter = wgetch(pad);



      // Prints the letters inside the box
      static short current_search_lines = 0;
      static short current_search_line_len = 0;
      if (current_search_lines <= 1 - 1 && current_search_line_len >= SEARCH_BOX_SIZE_X - 2) // Moves the current text a line up
      {
         pos.x -= current_search_line_len;

         for (int ii = 0; ii <= current_search_lines; ++ii) // Erase everything in the search box
         {
            wmove(pad, pos.y + ii, pos.x);

            for (int iii = 0; iii < current_search_line_len; ++iii)
            { 
               wprintw(pad, "%c", ' ');
            }
         }

         pos.y -= current_search_lines + 1;
         wmove(pad, pos.y + current_search_lines, pos.x);

         for (int ii = strlen(search_buffer) / current_search_line_len, step = 1; ii > 0; --ii) // Reprints everything
         {
            wmove(pad, pos.y + current_search_lines + step - 1, pos.x);

            for (int iii = current_search_line_len; iii > 0; --iii)
            {
               wprintw(pad, "%c", search_buffer[step * current_search_line_len - iii]);
            }

            ++step;
         }


         current_search_lines += strlen(search_buffer) / current_search_line_len;

         current_search_line_len = 0;
         // ++current_search_line_len; I don't think keeping this is a good idea

         wmove(pad, pos.y + current_search_lines, pos.x);
      }

      else if (current_search_line_len >= SEARCH_BOX_SIZE_X - 2 && current_search_lines < 2) // Maximum "SEARCH_BOX_SIZE_Y" has been reached, so it will move down. "current_search_lines < SEARCH_BOX_SIZE_Y - 2" makes it so that it doesn't go forever down
      {
         ++current_search_lines;

         if (current_search_lines == 2) { pos.x -= current_search_line_len; } // First time this "if" hits
         else { pos.x -= current_search_line_len - 1; }

         wmove(pad, pos.y + current_search_lines, pos.x);
         wprintw(pad, "%c", letter);

         current_search_line_len = 0;
         ++current_search_line_len;
      }

      else if (current_search_line_len < SEARCH_BOX_SIZE_X - 2)
      { 
         wprintw(pad, "%c", letter);
         ++pos.x;
         ++current_search_line_len;
      }

      else if (current_search_line_len >= SEARCH_BOX_SIZE_X - 2 && current_search_lines >= SEARCH_BOX_SIZE_Y - 3)
      {
         wmove(pad, pos.y + current_search_lines, pos.x);
         wprintw(pad, "%c", letter);
      }



      refresh_pad_custom(y, x); // This is here so that the "camera" doesn't move when writing to the box


      if (search_buffer_size <= i) // Every 10 iterations
      {
         search_buffer_size += 10;
         search_buffer = realloc(search_buffer, search_buffer_size);

         for (int ii = search_buffer_size - 10; ii <= search_buffer_size - 1; ++ii) // Ensures there's no garbage memory in the string. "search_buffer_size - 1" prevents an "invalid write of size 1"
         {
            search_buffer[ii] = '\0';
         }
      }

      if (letter == '\n')
      {
         search_buffer[i] = '\0';
         break;
      }


      if (letter != '\n' && (!backspace_pressed(letter)) && is_char_valid(letter) && letter != CTRL_A && letter != CTRL_F && letter != CTRL_X && letter != CTRL_W && letter != ESC)
      {
         search_buffer[i] = letter;
         ++i; // New letter added
      }

      else if (backspace_pressed(letter) && i > 0)
      {
         --i; // Letter removed
         search_buffer[i] = '\0';
      }

      else if (letter == ESC)
      {
         wattrset(pad, EDIT_MODE_COLOR);
         pos.y = y; pos.x = x; update_cursor_pos(); // I also return to the original position because finding "move_expressionY" and "move_expressionX" depends on position
         erase_search_box(text, position);
         pos.y = y; pos.x = x; update_cursor_pos();
         return; // Gets out of "search()"
      }
   }
   wattrset(pad, EDIT_MODE_COLOR);

   pos.y = y; pos.x = x; update_cursor_pos(); // I also return to the original position because finding "move_expressionY" and "move_expressionX" depends on position
   erase_search_box(text, position);
   pos.y = y; pos.x = x; update_cursor_pos(); // This line is here because in case the given substring is not found, the user will be back where he left off

   for (int i = pos.y; i < lines.current_total; ++i) // Scours all the text in search of the substring
   {
      if (i == pos.y) // A different way to search if its the 1st line (it doesn't count what's to the left of the cursor)
      {
         short found = 0; // 0 = not found; 1 = found
         int x = pos.x;

         for (int ii = 0, letters = 0; ii < strlen(text[pos.y]) - pos.x; ++ii, ++x)
         {
            if (text[pos.y][x] == search_buffer[letters]) { ++letters; }
            else { letters = 0; }

            if (letters == strlen(search_buffer))
            {
               found = 1;
               break;
            }
         }

         if (found == 0) { continue; }
      }
      else if (strstr(text[i], search_buffer) == NULL) { continue; } // Substring was not found


      const int srch_buf_len = strlen(search_buffer);

      pos.y = i;

      for (int ii = 0, letters = 0; ii < strlen(text[i]); ++ii) // Finds where to put "pos.x"
      {
         if (text[i][ii] == search_buffer[letters]) { ++letters; }
         else { letters = 0; }

         if (letters == srch_buf_len) // Substring found
         {
            pos.x = ii + 1;
            break;
         }
      }

      update_cursor_pos();
      break;
   }
}

void search_again(char** text)
{
   if (strcmp(search_buffer, "") == 0 || search_buffer == NULL) { return; }

   for (int i = pos.y; i < lines.current_total; ++i) // Scours all the text in search of the substring
   {
      if (i == pos.y) // A different way to search if its the 1st line (it doesn't count what's to the left of the cursor)
      {
         short found = 0; // 0 = not found; 1 = found
         int x = pos.x;

         for (int ii = 0, letters = 0; ii < strlen(text[pos.y]) - pos.x; ++ii, ++x)
         {
            if (text[pos.y][x] == search_buffer[letters]) { ++letters; }
            else { letters = 0; }

            if (letters == strlen(search_buffer))
            {
               found = 1;

               if (strlen(search_buffer) != 1) { pos.x = x; } // This is necessary if the "search_buffer" is a single letter (otherwise new "CTRL_N" presses won't work without moving to the right first)
               else { pos.x = x + 1; }

               update_cursor_pos();
               return;
            }
         }

         if (found == 0) { continue; }
      }
      else if (strstr(text[i], search_buffer) == NULL) { continue; } // Substring was not found


      const int srch_buf_len = strlen(search_buffer);

      pos.y = i;

      for (int ii = 0, letters = 0; ii < strlen(text[i]); ++ii) // Finds where to put "pos.x"
      {
         if (text[i][ii] == search_buffer[letters]) { ++letters; }
         else { letters = 0; }

         if (letters == srch_buf_len) // Substring found
         {
            pos.x = ii + 1;
            break;
         }
      }

      update_cursor_pos();
      break;
   }
}
