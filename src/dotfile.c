#include "../include/dotfile.h"
#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ncurses.h>

#define DOTFILE_SIZE 375 // This macro defines the current size of the dotfile. This must be updated MANUALLY before adding any new features to the file (note: this value MUST be bigger than the actual file size, as the user might type in values that are longer than the current ones. EX: "black" is a longer value than "red")

extern struct DotFileData dotfile_data;

char* get_dotfile_location(char* dir_or_file)
{
   char* path;
   const char* const xdg_config_env = getenv("XDG_CONFIG_HOME");


   if ((xdg_config_env == NULL) || (xdg_config_env[strlen(xdg_config_env) - 1] == '/')) // If the env var "XDG_CONFIG_HOME" is not set OR if it ends with a slash (which is bad and not the standard)
   {
      char* username = getlogin();
      path = malloc(300); strcpy(path, "/home/"); // Since I've allocated 300 bytes to "path", it can work with usernames up to 279 bytes. (6 bytes from "/home/" + 279 from the username + 13 from "/.config/zx/")
      strcat(path, username);
      strcat(path, "/.config/zx/");
   }

   else
   {
      path = calloc(strlen(getenv("XDG_CONFIG_HOME")) + 12, 1);
      strcpy(path, xdg_config_env);
      strcat(path, "/zx/");
   }


   if (strcmp(dir_or_file, "DIR") == 0) { return path; } // Returns the directory's PATH
   else if (strcmp(dir_or_file, "FILE") == 0) { strcat(path, "zx.conf"); return path; } // Returns the file's PATH
   else { return NULL; }
}

int check_dotfile()
{
   char* path = get_dotfile_location("DIR");

   DIR *dotfile_dir = opendir(path);
   if (dotfile_dir == NULL) // If the directory does not exist
   {
      char* mkdir_command = malloc(strlen(path) + 10);
      strcpy(mkdir_command, "mkdir -p ");
      strcat(mkdir_command, path);
      int return_code = system(mkdir_command);
      free(mkdir_command);

      switch (return_code)
      {
      case 256:
         free(path);
         wmove(stdscr, LINES / 2, (COLS / 2) - 39);
         wprintw(stdscr, "ERROR: NOT ENOUGH PRIVILEGES TO CREATE THE DOTFILE DIRECTORY IN THIS LOCATION!");
         getch();
         return return_code;

      case 0:
         break;

      default:
         free(path);
         wmove(stdscr, LINES / 2, (COLS / 2) - 32);
         wprintw(stdscr, "ERROR: SOMETHING WENT WRONG WHEN CREATING THE DOTFILE DIRECTORY!");
         getch();
         return return_code;
      }
   }
   closedir(dotfile_dir);


   strcat(path, "zx.conf");

   FILE *dotfile = fopen(path, "r");
   if (dotfile == NULL) // If the file does not exist
   {
      dotfile = fopen(path, "a");
      fprintf(dotfile, "foreground-color:white\nbackground-color:black\nedit-foreground-color:cyan\nedit-background-color:black\n\nctrl-up-steps:10\nctrl-left-steps:10\nctrl-right-steps:10\nctrl-down-steps:10\n\ncarry-lines:true\n\nsave-color:true\nsave-foreground-color:green\nsave-background-color:black\n\ncursor-centralized:true\n");
   }

   free(path);
   fclose(dotfile);

   return 0;
}

void read_dotfile()
{
   char* path = get_dotfile_location("FILE");

   FILE *dotfile = fopen(path, "r");
   char* data = malloc(DOTFILE_SIZE);
   free(path);



   // Assigns all data from the file to the struct
   // Foreground Color
   dotfile_data.foreground_color = malloc(10);
   fgets(data, DOTFILE_SIZE, dotfile);
   int found_colon = 0;
   char* value = calloc(10, 1);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   strcpy(dotfile_data.foreground_color, value);

   // Background Color
   dotfile_data.background_color = malloc(10);
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   strcpy(dotfile_data.background_color, value);

   // Edit Foreground Color
   dotfile_data.edit_foreground_color = malloc(10);
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   strcpy(dotfile_data.edit_foreground_color, value);

   // Edit Background Color
   dotfile_data.edit_background_color = malloc(10);
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   strcpy(dotfile_data.edit_background_color, value);

   fgets(data, DOTFILE_SIZE, dotfile); // This line is just an empty one

   // Ctrl Up Steps
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   dotfile_data.ctrl_up_steps = atoi(value);

   // Ctrl Left Steps
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   dotfile_data.ctrl_left_steps = atoi(value);

   // Ctrl Right Steps
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   dotfile_data.ctrl_right_steps = atoi(value);

   // Ctrl Down Steps
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   dotfile_data.ctrl_down_steps = atoi(value);

   fgets(data, DOTFILE_SIZE, dotfile); // This line is just an empty one

   // Carry Lines
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   if (strcmp(value, "false") == 0) { dotfile_data.carry_lines = 0; }
   else { dotfile_data.carry_lines = 1; }

   fgets(data, DOTFILE_SIZE, dotfile); // This line is just an empty one

   // Whether or not to use save colors
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   if (strcmp(value, "false") == 0) { dotfile_data.save_color = 0; }
   else { dotfile_data.save_color = 1; }

   // Save Foreground Color
   dotfile_data.save_foreground_color = malloc(10);
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   strcpy(dotfile_data.save_foreground_color, value);

   // Save Background Color
   dotfile_data.save_background_color = malloc(10);
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   strcpy(dotfile_data.save_background_color, value);

   fgets(data, DOTFILE_SIZE, dotfile); // This line is just an empty one

   // Cursor Centralized
   fgets(data, DOTFILE_SIZE, dotfile);
   found_colon = 0;
   memset(value, 0, 10);
   for (int i = 0, ii = 0; i < strlen(data) - 1; ++i) // This has "strlen - 1" because I don't want to count the "\n" at the end
   {
      if (found_colon == 1)
      {
         value[ii] = data[i];
         ++ii;
      }

      if (data[i] == ':') { found_colon = 1; }
   }
   if (strcmp(value, "false") == 0) { dotfile_data.cursor_centralized = 0; }
   else { dotfile_data.cursor_centralized = 1; }

   free(value);
   free(data);
}

void setup_color_pairs()
{
   int foreground_color = 0;
   int background_color = 0;

   // Foreground color
   if (strcmp(dotfile_data.foreground_color, "black") == 0) { foreground_color = COLOR_BLACK; }
   else if (strcmp(dotfile_data.foreground_color, "red") == 0) { foreground_color = COLOR_RED; }
   else if (strcmp(dotfile_data.foreground_color, "green") == 0) { foreground_color = COLOR_GREEN; }
   else if (strcmp(dotfile_data.foreground_color, "yellow") == 0) { foreground_color = COLOR_YELLOW; }
   else if (strcmp(dotfile_data.foreground_color, "blue") == 0) { foreground_color = COLOR_BLUE; }
   else if (strcmp(dotfile_data.foreground_color, "magenta") == 0) { foreground_color = COLOR_MAGENTA; }
   else if (strcmp(dotfile_data.foreground_color, "cyan") == 0) { foreground_color = COLOR_CYAN; }
   else if (strcmp(dotfile_data.foreground_color, "white") == 0) { foreground_color = COLOR_WHITE; }

   // Background color
   if (strcmp(dotfile_data.background_color, "black") == 0) { background_color = COLOR_BLACK; }
   else if (strcmp(dotfile_data.background_color, "red") == 0) { background_color = COLOR_RED; }
   else if (strcmp(dotfile_data.background_color, "green") == 0) { background_color = COLOR_GREEN; }
   else if (strcmp(dotfile_data.background_color, "yellow") == 0) { background_color = COLOR_YELLOW; }
   else if (strcmp(dotfile_data.background_color, "blue") == 0) { background_color = COLOR_BLUE; }
   else if (strcmp(dotfile_data.background_color, "magenta") == 0) { background_color = COLOR_MAGENTA; }
   else if (strcmp(dotfile_data.background_color, "cyan") == 0) { background_color = COLOR_CYAN; }
   else if (strcmp(dotfile_data.background_color, "white") == 0) { background_color = COLOR_WHITE; }


   init_pair(1, foreground_color, background_color); // NORMAL mode color



   int edit_foreground_color = 0;
   int edit_background_color = 0;

   // EDIT foreground color
   if (strcmp(dotfile_data.edit_foreground_color, "black") == 0) { edit_foreground_color = COLOR_BLACK; }
   else if (strcmp(dotfile_data.edit_foreground_color, "red") == 0) { edit_foreground_color = COLOR_RED; }
   else if (strcmp(dotfile_data.edit_foreground_color, "green") == 0) { edit_foreground_color = COLOR_GREEN; }
   else if (strcmp(dotfile_data.edit_foreground_color, "yellow") == 0) { edit_foreground_color = COLOR_YELLOW; }
   else if (strcmp(dotfile_data.edit_foreground_color, "blue") == 0) { edit_foreground_color = COLOR_BLUE; }
   else if (strcmp(dotfile_data.edit_foreground_color, "magenta") == 0) { edit_foreground_color = COLOR_MAGENTA; }
   else if (strcmp(dotfile_data.edit_foreground_color, "cyan") == 0) { edit_foreground_color = COLOR_CYAN; }
   else if (strcmp(dotfile_data.edit_foreground_color, "white") == 0) { edit_foreground_color = COLOR_WHITE; }

   // EDIT background color
   if (strcmp(dotfile_data.edit_background_color, "black") == 0) { edit_background_color = COLOR_BLACK; }
   else if (strcmp(dotfile_data.edit_background_color, "red") == 0) { edit_background_color = COLOR_RED; }
   else if (strcmp(dotfile_data.edit_background_color, "green") == 0) { edit_background_color = COLOR_GREEN; }
   else if (strcmp(dotfile_data.edit_background_color, "yellow") == 0) { edit_background_color = COLOR_YELLOW; }
   else if (strcmp(dotfile_data.edit_background_color, "blue") == 0) { edit_background_color = COLOR_BLUE; }
   else if (strcmp(dotfile_data.edit_background_color, "magenta") == 0) { edit_background_color = COLOR_MAGENTA; }
   else if (strcmp(dotfile_data.edit_background_color, "cyan") == 0) { edit_background_color = COLOR_CYAN; }
   else if (strcmp(dotfile_data.edit_background_color, "white") == 0) { edit_background_color = COLOR_WHITE; }


   init_pair(2, edit_foreground_color, edit_background_color); // EDIT mode color



   int save_foreground_color = 0;
   int save_background_color = 0;

   // Save foreground color
   if (strcmp(dotfile_data.save_foreground_color, "black") == 0) { save_foreground_color = COLOR_BLACK; }
   else if (strcmp(dotfile_data.save_foreground_color, "red") == 0) { save_foreground_color = COLOR_RED; }
   else if (strcmp(dotfile_data.save_foreground_color, "green") == 0) { save_foreground_color = COLOR_GREEN; }
   else if (strcmp(dotfile_data.save_foreground_color, "yellow") == 0) { save_foreground_color = COLOR_YELLOW; }
   else if (strcmp(dotfile_data.save_foreground_color, "blue") == 0) { save_foreground_color = COLOR_BLUE; }
   else if (strcmp(dotfile_data.save_foreground_color, "magenta") == 0) { save_foreground_color = COLOR_MAGENTA; }
   else if (strcmp(dotfile_data.save_foreground_color, "cyan") == 0) { save_foreground_color = COLOR_CYAN; }
   else if (strcmp(dotfile_data.save_foreground_color, "white") == 0) { save_foreground_color = COLOR_WHITE; }

   // Save background color
   if (strcmp(dotfile_data.save_background_color, "black") == 0) { save_background_color = COLOR_BLACK; }
   else if (strcmp(dotfile_data.save_background_color, "red") == 0) { save_background_color = COLOR_RED; }
   else if (strcmp(dotfile_data.save_background_color, "green") == 0) { save_background_color = COLOR_GREEN; }
   else if (strcmp(dotfile_data.save_background_color, "yellow") == 0) { save_background_color = COLOR_YELLOW; }
   else if (strcmp(dotfile_data.save_background_color, "blue") == 0) { save_background_color = COLOR_BLUE; }
   else if (strcmp(dotfile_data.save_background_color, "magenta") == 0) { save_background_color = COLOR_MAGENTA; }
   else if (strcmp(dotfile_data.save_background_color, "cyan") == 0) { save_background_color = COLOR_CYAN; }
   else if (strcmp(dotfile_data.save_background_color, "white") == 0) { save_background_color = COLOR_WHITE; }


   init_pair(3, save_foreground_color, save_background_color); // Save color


   init_pair(5, COLOR_RED, COLOR_BLUE); // Search box (for now it's not customizable)
   init_pair(6, COLOR_RED, COLOR_BLUE); // Search box FG (for now it's not customizable)
}
