#ifndef PAD_CHECK_H
#define PAD_CHECK_H

// These 2 functions require a "mode" parameter: 
   // On "check_pad_size_X()" to check whether or not the "current_line" needs to be printed + the color that needs to be set
   // On "check_pad_size_Y()" only to check for the color that needs to be set

// NOTE on "check_pad_size_X()": (If on NORMAL mode, the "current_line" is not yet part of **text, so it DOES need to be printed. On EDIT mode, "current_line" is already part of the **text, so it does NOT need to be printed)

void check_pad_size_Y(char** text, int mode); // Will check if the pad needs to be recreated with a bigger size in the Y position
void check_pad_size_X(char** text, char* current_line, int mode); // Will check if the pad needs to be recreated with a bigger size in the X position. It requires a "current_line" param because I call it amidst getting a new line (so it won't be stored in **text at that moment)

#endif