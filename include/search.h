#ifndef SEARCH_H
#define SEARCH_H

short draw_search_box(); // Draws the search box at the center of the screen. Returns where the box should be drawn, which I pass to "erase_search_box()"
void erase_search_box(char** text, short position);

void search(char** text); // Allow the user to search for a substring
void search_again(char** text); // Searches for the previously typed substring once again

#endif
