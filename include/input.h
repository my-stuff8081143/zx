#ifndef INPUT_H
#define INPUT_H

int backspace_pressed(char input); // Checks if backspace was pressed or not. Returns 1 if it was pressed, and 0 if it was not
void refresh_pad(); // Refreshes the pad. Checks if scrolling is needed or not
void refresh_pad_custom(int y, int x); // This function only exists for me to use it on "search.c" with search box-related shenanigans
int is_char_valid(int input); // Checks if a given char is valid. Examples of non-valid: mouse wheel scroll, non handled ctrl+<char>, F<1-12> etc.
int get_line(char** text, char* string, int max_length, char* file_path); // Gets a line of text input and assigns it to "text". The char returned is only used for ctrl_input checking. "file_path" is here for the sake of "CTRL+A" to save the file
void get_input(char** text, char* file_path); // Gets input and stores it in the "text" double pointer. "file_path" is here for the sake of "CTRL+A" to save the file

#endif
