#ifndef LINES_STRUCT_H
#define LINES_STRUCT_H

// This struct keeps track of the total amount of lines in the txt file
struct Lines
{
   int current_total; // The current total amount of lines in the text file
   int peak_total; // The "peak" total amount of lines in a text file (this value can be higher than "current_total" because the user can erase many lines)
   int longest_line_length; // The length of the longest line
};

int is_peak_lines_reached(int current_total, int peak_total); // Checks if "current_total" is bigger than "peak_total". If so, "peak_total" will equal "current_total"
int is_longest_line(char* line, int longest_length); // Checks if the length of "line" is bigger than "longest_length", if so, returns the length of "line". Else, it will return "longest_length" back. In any way, the longest length will get returned

#endif