#include <string.h>
#include "./lines-struct.h"

int is_peak_lines_reached(int current_total, int peak_total)
{
   if (current_total > peak_total) { peak_total = current_total; }
   return peak_total;
}

int is_longest_line(char* line, int longest_length)
{
   int line_length = strlen(line);
   if (line_length > longest_length)
   {
      return line_length;
   }

   return longest_length;
}