#ifndef DOTFILE_H
#define DOTFILE_H

struct DotFileData
{
   char* foreground_color;
   char* background_color;
   char* edit_foreground_color;
   char* edit_background_color;

   int ctrl_up_steps;
   int ctrl_left_steps;
   int ctrl_right_steps;
   int ctrl_down_steps;

   int carry_lines; // 0 for false, 1 for true

   int save_color; // 0 for false, 1 for true
   char* save_foreground_color;
   char* save_background_color;

   int cursor_centralized; // 0 for false, 1 for true
};

char* get_dotfile_location(char* dir_or_file); // Returns the PATH of the dotfile. The "directory" arg can be "0" for retrieving the file location or "1" for retrieving the directory location

int check_dotfile(); // Checks if both the dotdirectory and dotfile exists. If they do not, they get created
void read_dotfile(); // Reads the dotfile and assigns the data to "dotfile_data"

void setup_color_pairs(); // Checks what color settings are present in the dotfile

#endif
