#ifndef TUI_H
#define TUI_H

void print_BACK_btn();


void update_select_color_pair();


void recreate_dotfile_with_new_setting(short setting, char* value);


void print_BLACK_btn();
void print_RED_btn();
void print_GREEN_btn();
void print_YELLOW_btn();
void print_BLUE_btn();
void print_MAGENTA_btn();
void print_CYAN_btn();
void print_WHITE_btn();
void display_color_selection_menu(short setting);


void get_ctrl_steps(short setting);


void print_TRUE_btn();
void print_FALSE_btn();
void display_bool_selection_menu(short setting);


void print_FOREGROUND_COLOR_btn();
void print_BACKGROUND_COLOR_btn();
void print_EDIT_FOREGROUND_COLOR_btn();
void print_EDIT_BACKGROUND_COLOR_btn();
void print_CTRLUP_btn();
void print_CTRLLEFT_btn();
void print_CTRLRIGHT_btn();
void print_CTRLDOWN_btn();
void print_CARRY_LINES_btn();
void print_SAVE_COLOR_btn();
void print_SAVE_FOREGROUND_COLOR_btn();
void print_SAVE_BACKGROUND_COLOR_btn();
void display_settings();


void print_OPEN_FILE_btn();
void print_SETTINGS_btn();
void print_QUIT_btn();
short display_tui();

#endif
