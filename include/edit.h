#ifndef EDIT_H
#define EDIT_H

void update_cursor_pos();
int move_cursor(char* current_text_line); // This parameter is here solely for the sake of "Shift+Right_arrow" so that the cursor can move to the end of the current line

void add_char(char** text, int input);
void remove_char(char** text);
void remove_word(char** text);

int add_empty_line(char** text, int total_lines); // Adds empty lines on previously typed text under EDIT mode. Returns 0 on success. This func has a param "total_lines" because I can't just use the global "lines.current_total", as I don't want the changes I make to the lines variable within this func to last
int remove_empty_line(char** text, int total_lines); // Removes empty lines on previously typed text under EDIT mode. Returns 0 or 1 on success (0 = func executed normally. 1 = func removed the first line). This func has a param "total_lines" because I can't just use the global "lines.current_total", as I don't want the changes I make to the lines variable within this func to last

int edit(char** text, const char* const file_path); // Returns 360 if user types in "END". Otherwise returns nothing meaningful. "file_path" is here for the sake of "CTRL+A" to save the file

#endif
